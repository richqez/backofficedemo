package com.tgs.bc.utill;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.tgs.bc.dto.MsgStatus;

public class CommonUtill  {
	

	public static void throwFacesMessage(MsgStatus msgStatus) {
		
		if (msgStatus.getStatus()) {

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO,msgStatus.getStatusStr(),msgStatus.getMessage()));

		}
		else
		{
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL,msgStatus.getStatusStr(),msgStatus.getMessage()));
		}
	}
	
	

	

}
