package com.tgs.bc.utill;

import com.tgs.bc.model.Workright;

public interface WorkRightFactory {
	
	public final static String ADD_PERMISSION = "ADD";
	public final static String UPDATE_PERMISSION = "UPDATE";
	public final static String DELETE_PERMISSION = "DELETE";
	
	public WorkRightFactory getWorkRight();
	
	public Workright setPermisstion(String permissionCode);
	
}
