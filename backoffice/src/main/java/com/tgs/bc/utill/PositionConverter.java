package com.tgs.bc.utill;

import java.util.Collection;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tgs.bc.model.Position;
import com.tgs.bc.service.CommonSv;


@FacesConverter(value = "converterValue2Item")
public class PositionConverter implements Converter {
	
	@Autowired 
	CommonSv commonSv;
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		Object selected = null;
		try {
			if (value == null)
				return selected;

			Collection<UIComponent> childComponents = component.getChildren();
			for (UIComponent childComponent : childComponents) {
				if (childComponent.getClass().equals(UISelectItems.class)) {
					Collection<?> values = (Collection<?>) childComponent.getValueExpression("value").getValue(FacesContext.getCurrentInstance().getELContext());

					for (Object c : values) {
						if (value.equals(c.toString())) {
							selected = c;
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return selected;
		
	
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object o) {
			
		if (o == null || o.toString() == "") {

			return "";
		} else {
			if (o instanceof Object) {
//				logger.info("instanceof Object");
				
				return o.toString();
			} else {
//				logger.error("object " + o + " expected type: String");

				throw new IllegalArgumentException("object " + o + " is of type " + o.getClass().getName() + "; expected type: String");
			}
		}
	}

	
	

	
}
