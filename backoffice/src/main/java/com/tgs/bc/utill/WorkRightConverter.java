package com.tgs.bc.utill;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tgs.bc.model.Workright;
import com.tgs.bc.service.CommonSv;

@Component
@Scope("session")
public class WorkRightConverter implements Converter {

	@Autowired 
	CommonSv commonSv ;
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		Workright workright = (Workright) commonSv.findByIdCode
				(Workright.class,value);
		
		return workright;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value == null || value == "") {
			
			
			return "";
		}

		Workright workright = (Workright)value;

		return workright.getCode() != "" ? workright.getCode()
				: null;
	}

	
	
}
