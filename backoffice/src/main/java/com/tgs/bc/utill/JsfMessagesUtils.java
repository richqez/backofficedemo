package com.tgs.bc.utill;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

import com.tgs.bc.dto.MsgStatus;

/**
 * @author Taiy
 *
 *
 *
 *         Chained method syle
 *
 *
 */
public class JsfMessagesUtils {

	private Severity severity = FacesMessage.SEVERITY_ERROR;

	private String bodymessage = "no-message";

	private String headmessage = "";

	public static JsfMessagesUtils getFacessMessage() {
		return new JsfMessagesUtils();
	}

	public void show() {
		FacesContext.getCurrentInstance().addMessage(
				null,
				new FacesMessage(this.severity, this.headmessage,
						this.bodymessage));
	}

	public JsfMessagesUtils setMsgStatus(MsgStatus msgStatus) {
		this.headmessage = msgStatus.getStatusStr();
		this.bodymessage = msgStatus.getMessage();

		if (msgStatus.getStatus()) {
			setInfoMessage();
		} else {
			setFatalMessage();
		}

		return this;
	}

	public JsfMessagesUtils setHeadMessage(String headMessage) {
		this.headmessage = headMessage;
		return this;
	}

	public JsfMessagesUtils setBodyMessage(String bodyMessage) {
		this.bodymessage = bodyMessage;
		return this;
	}

	public JsfMessagesUtils setErrorMessage() {
		this.severity = FacesMessage.SEVERITY_ERROR;
		return this;
	}

	public JsfMessagesUtils setFatalMessage() {
		this.severity = FacesMessage.SEVERITY_FATAL;
		return this;
	}

	public JsfMessagesUtils setInfoMessage() {
		this.severity = FacesMessage.SEVERITY_INFO;
		return this;
	}

	public JsfMessagesUtils setWarnMessage() {
		this.severity = FacesMessage.SEVERITY_WARN;
		return this;
	}

}
