package com.tgs.bc.utill;

import java.util.Collection;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tgs.bc.model.EmployeeInfo;
import com.tgs.bc.service.CommonSv;


@FacesConverter(value = "converterValue2Item")
public class EmployeeConverter implements Converter {

	@Autowired
	CommonSv commonSv;

	public Object getAsObject(FacesContext facesContext, UIComponent uiComponent, String value){

		Object selected = null;
		try {
			if (value == null)
				return selected;

			Collection<UIComponent> childComponents = uiComponent.getChildren();
			for (UIComponent childComponent : childComponents) {
				if (childComponent.getClass().equals(UISelectItems.class)) {
					Collection<?> values = (Collection<?>) childComponent.getValueExpression("value").getValue(FacesContext.getCurrentInstance().getELContext());

					for (Object c : values) {
						if (value.equals(c.toString())) {
							selected = c;
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
		return selected;
	}
	
	public String getAsString(FacesContext facesContext, UIComponent ui, Object o) {
		if (o == null || o.toString() == "") {

			return "";
		} else {
			if (o instanceof Object) {
//			
				
				return o.toString();
			} else {
//				

				throw new IllegalArgumentException("object " + o + " is of type " + o.getClass().getName() + "; expected type: String");
			}
		}
	}

}
