package com.tgs.bc.utill;

import com.tgs.bc.model.Workright;

public class WorkRightFactoryImpl implements WorkRightFactory{
	
	Workright workright ;
	
	public WorkRightFactory getWorkRight(){
		workright = new Workright();
		return this ;
	}
	
	public Workright setPermisstion(String permissionCode)
	{
		workright.setCode(permissionCode);
		return workright;
	}
	
	
}
