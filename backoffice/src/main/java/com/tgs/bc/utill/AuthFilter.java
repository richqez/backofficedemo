package com.tgs.bc.utill;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tgs.bc.controller.LoginCtl;
import com.tgs.bc.controller.UserCtl;


public class AuthFilter implements Filter {

	private static final String LOGIN_PAGE = "/login.xhtml";
	

	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,
			FilterChain arg2) throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) arg0 ;
		HttpServletResponse httpServletResponse = (HttpServletResponse) arg1;
		
		
		LoginCtl loginCtl = (LoginCtl) httpServletRequest.getSession().getAttribute("loginCtl");
		
		if(loginCtl!=null){
			arg2.doFilter(httpServletRequest,httpServletResponse);
		}else
		{
			httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + LOGIN_PAGE);
		}
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
