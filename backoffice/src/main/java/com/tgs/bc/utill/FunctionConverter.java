package com.tgs.bc.utill;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tgs.bc.model.Function;
import com.tgs.bc.service.CommonSv;
@Component
@Scope("session")
public class FunctionConverter implements Converter {
	
	@Autowired
	CommonSv commonSv ;
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		Function function = (Function) commonSv.findById(Function.class,value); 
		return function;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		if (value==null) {
			return "";
		}

		Function function = (Function) value;
		
		return function.getId() != -1 ? String.valueOf(function.getId()) : null ;
	}

}
