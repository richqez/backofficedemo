package com.tgs.bc.utill;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.tgs.bc.model.Menu;
import com.tgs.bc.service.CommonSv;

@Component
@Scope("session")
public class MenuConverter implements Converter {

	@Autowired
	CommonSv commonSv ;
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		
		if (value=="-99" || value.equals("-99")) {
			Menu menu = new Menu();
			menu.setId(-99);
			menu.setMenu(null);
			
			return menu;
		}
		
		Menu menu = (Menu) commonSv.findById(Menu.class,value);
		
		return menu;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		if (value==null) {
			return "";
		}
		
		Menu menu = (Menu)value;
		
		return menu.getId() != 0 ? String.valueOf(menu.getId()) :null ;
		
		
	}

}
