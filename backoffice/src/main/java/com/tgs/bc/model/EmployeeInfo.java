package com.tgs.bc.model;

// Generated 23-Jul-2015 10:13:20 by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * EmployeeInfo generated by hbm2java
 */
@Entity
@Table(name = "employee_info", schema = "backoffice")
public class EmployeeInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3636984590852039306L;
	private int id;
	private Position position;
	private String firstName;
	private String lastName;
	private Date birthDate;
	private String age;
	private String address;
	private String telMobileNumber;
	private String telHomeNumber;
	private String empStatus;
	private Character gender;
	private String createdBy;
	private String updatedBy;
	private Date createdDate;
	private Date updatedDate;
	private Date startWorkDate;
	private Date endWorkDate;
	private Set<User> users = new HashSet<User>(0);

	public EmployeeInfo() {
	}

	public EmployeeInfo(int id) {
		this.id = id;
	}

	public EmployeeInfo(int id, Position position, String firstName,
			String lastName, Date birthDate, String age, String address,
			String telMobileNumber, String telHomeNumber, String empStatus,
			Character gender, String createdBy, String updatedBy,
			Date createdDate, Date updatedDate, Date startWorkDate,
			Date endWorkDate, Set<User> users) {
		this.id = id;
		this.position = position;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.age = age;
		this.address = address;
		this.telMobileNumber = telMobileNumber;
		this.telHomeNumber = telHomeNumber;
		this.empStatus = empStatus;
		this.gender = gender;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.startWorkDate = startWorkDate;
		this.endWorkDate = endWorkDate;
		this.users = users;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position_id")
	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Column(name = "first_name")
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "last_name")
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birth_date", length = 13)
	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "age")
	public String getAge() {
		return this.age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Column(name = "address")
	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "tel_mobile_number")
	public String getTelMobileNumber() {
		return this.telMobileNumber;
	}

	public void setTelMobileNumber(String telMobileNumber) {
		this.telMobileNumber = telMobileNumber;
	}

	@Column(name = "tel_home_number")
	public String getTelHomeNumber() {
		return this.telHomeNumber;
	}

	public void setTelHomeNumber(String telHomeNumber) {
		this.telHomeNumber = telHomeNumber;
	}

	@Column(name = "emp_status")
	public String getEmpStatus() {
		return this.empStatus;
	}

	public void setEmpStatus(String empStatus) {
		this.empStatus = empStatus;
	}

	@Column(name = "gender", length = 1)
	public Character getGender() {
		return this.gender;
	}

	public void setGender(Character gender) {
		this.gender = gender;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 13)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 13)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "start_work_date", length = 13)
	public Date getStartWorkDate() {
		return this.startWorkDate;
	}

	public void setStartWorkDate(Date startWorkDate) {
		this.startWorkDate = startWorkDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "end_work_date", length = 13)
	public Date getEndWorkDate() {
		return this.endWorkDate;
	}

	public void setEndWorkDate(Date endWorkDate) {
		this.endWorkDate = endWorkDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employeeInfo")
	public Set<User> getUsers() {
		return this.users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}


	@Transient
	@Override
	public boolean equals(Object obj) {
	     return (obj instanceof EmployeeInfo) 
	          ? getId()==(((EmployeeInfo) obj).getId())
	          : (obj == this);  
	 }
}
