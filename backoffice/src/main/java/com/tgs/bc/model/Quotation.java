package com.tgs.bc.model;

// Generated 23-Jul-2015 10:13:20 by Hibernate Tools 4.3.1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * Quotation generated by hbm2java
 */
@Entity
@Table(name = "quotation", schema = "backoffice", uniqueConstraints = @UniqueConstraint(columnNames = "qt_number"))
public class Quotation implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7150165898692856125L;
	private int id;
	private Customer customer;
	private String qtNumber;
	private Integer templateId;
	private String status;
	private String referrence;
	private Date docDate;
	private String remarks;
	private String preparedBy;
	private String organizerBy;
	private String appvoredBy;
	private Double total;
	private Double discountPercentage;
	private Double discount;
	private Boolean vatStatus;
	private Double vatPercentage;
	private Double vat;
	private Double grandTotal;
	private String createdBy;
	private String updatedBy;
	private Date createdDate;
	private Date updatedDate;
	private Set<QuotationDetail> quotationDetails = new HashSet<QuotationDetail>(
			0);

	public Quotation() {
	}

	public Quotation(int id) {
		this.id = id;
	}

	public Quotation(int id, Customer customer, String qtNumber,
			Integer templateId, String status, String referrence, Date docDate,
			String remarks, String preparedBy, String organizerBy,
			String appvoredBy, Double total, Double discountPercentage,
			Double discount, Boolean vatStatus, Double vatPercentage,
			Double vat, Double grandTotal, String createdBy, String updatedBy,
			Date createdDate, Date updatedDate,
			Set<QuotationDetail> quotationDetails) {
		this.id = id;
		this.customer = customer;
		this.qtNumber = qtNumber;
		this.templateId = templateId;
		this.status = status;
		this.referrence = referrence;
		this.docDate = docDate;
		this.remarks = remarks;
		this.preparedBy = preparedBy;
		this.organizerBy = organizerBy;
		this.appvoredBy = appvoredBy;
		this.total = total;
		this.discountPercentage = discountPercentage;
		this.discount = discount;
		this.vatStatus = vatStatus;
		this.vatPercentage = vatPercentage;
		this.vat = vat;
		this.grandTotal = grandTotal;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.quotationDetails = quotationDetails;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "customer_id")
	public Customer getCustomer() {
		return this.customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Column(name = "qt_number", unique = true)
	public String getQtNumber() {
		return this.qtNumber;
	}

	public void setQtNumber(String qtNumber) {
		this.qtNumber = qtNumber;
	}

	@Column(name = "template_id")
	public Integer getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	@Column(name = "status")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "referrence")
	public String getReferrence() {
		return this.referrence;
	}

	public void setReferrence(String referrence) {
		this.referrence = referrence;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "doc_date", length = 13)
	public Date getDocDate() {
		return this.docDate;
	}

	public void setDocDate(Date docDate) {
		this.docDate = docDate;
	}

	@Column(name = "remarks")
	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Column(name = "prepared_by")
	public String getPreparedBy() {
		return this.preparedBy;
	}

	public void setPreparedBy(String preparedBy) {
		this.preparedBy = preparedBy;
	}

	@Column(name = "organizer_by")
	public String getOrganizerBy() {
		return this.organizerBy;
	}

	public void setOrganizerBy(String organizerBy) {
		this.organizerBy = organizerBy;
	}

	@Column(name = "appvored_by")
	public String getAppvoredBy() {
		return this.appvoredBy;
	}

	public void setAppvoredBy(String appvoredBy) {
		this.appvoredBy = appvoredBy;
	}

	@Column(name = "total", precision = 17, scale = 17)
	public Double getTotal() {
		return this.total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	@Column(name = "discount_percentage", precision = 17, scale = 17)
	public Double getDiscountPercentage() {
		return this.discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Column(name = "discount", precision = 17, scale = 17)
	public Double getDiscount() {
		return this.discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	@Column(name = "vat_status")
	public Boolean getVatStatus() {
		return this.vatStatus;
	}

	public void setVatStatus(Boolean vatStatus) {
		this.vatStatus = vatStatus;
	}

	@Column(name = "vat_percentage", precision = 17, scale = 17)
	public Double getVatPercentage() {
		return this.vatPercentage;
	}

	public void setVatPercentage(Double vatPercentage) {
		this.vatPercentage = vatPercentage;
	}

	@Column(name = "vat", precision = 17, scale = 17)
	public Double getVat() {
		return this.vat;
	}

	public void setVat(Double vat) {
		this.vat = vat;
	}

	@Column(name = "grand_total", precision = 17, scale = 17)
	public Double getGrandTotal() {
		return this.grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	@Column(name = "created_by")
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "updated_by")
	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "created_date", length = 13)
	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "updated_date", length = 13)
	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "quotation")
	public Set<QuotationDetail> getQuotationDetails() {
		return this.quotationDetails;
	}

	public void setQuotationDetails(Set<QuotationDetail> quotationDetails) {
		this.quotationDetails = quotationDetails;
	}

}
