package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Quotation;
import com.tgs.bc.model.QuotationDetail;

@Repository
@Scope("singleton")
public class QuotationDaoImpl  implements QuotationDao {

	@Autowired
	SessionFactory sessionFactoy;

	@SuppressWarnings("unchecked")
	public List<Quotation> findByCriteria(Quotation quotation) {

		Criteria criteria = sessionFactoy.getCurrentSession().createCriteria(
				Quotation.class);

		if (quotation.getQtNumber() != null && quotation.getQtNumber() != "") {
			criteria.add(Restrictions.like("qtNumber", quotation.getQtNumber(),
					MatchMode.ANYWHERE));
		} else if (quotation.getCustomer() != null) {

			if (quotation.getCustomer().getName() != null
					&& quotation.getCustomer().getName() != "") {
				criteria.createCriteria("customer").add(
						Restrictions.like("name", quotation.getCustomer()
								.getName(), MatchMode.ANYWHERE));

			} else if (quotation.getCustomer().getCompanyName() != null
					&& quotation.getCustomer().getCompanyName() != "") {
				criteria.createCriteria("customer").add(
						Restrictions.like("company", quotation.getCustomer()
								.getCompanyName(), MatchMode.ANYWHERE));
			}
		}

		return criteria.setFetchMode("customer", FetchMode.JOIN).list();
	}

	@SuppressWarnings("unchecked")
	public List<QuotationDetail> findQuotationDetailByQuotation(
			Quotation quotation) {

		return sessionFactoy.getCurrentSession()
				.createCriteria(QuotationDetail.class)
				.add(Restrictions.eq("quotation", quotation))
				.addOrder(Order.asc("id"))
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quotation> findByCriteria(
			CommonSearchDto<Quotation> commonSearchDto) {
		Criteria criteria = sessionFactoy.getCurrentSession().createCriteria(
				Quotation.class);
		
		if (commonSearchDto.getObject().getQtNumber() != ""
				&& commonSearchDto.getObject().getQtNumber() != null) {
			criteria.add(Restrictions.eq("qtNumber", commonSearchDto
					.getObject().getQtNumber()));
		}else if (commonSearchDto.getCriteria() != null) {

			// Case Search By Start Date
		}
		if (commonSearchDto.getCriteria().containsKey(
				CommonSearchDto.START_DATE)
				&& !commonSearchDto.getCriteria().containsKey(
						CommonSearchDto.END_DATE)) {
			criteria.add(Restrictions.ge("docDate", commonSearchDto
					.getCriteria().get(CommonSearchDto.START_DATE)));
		}// Case Search By End Date
		else if (commonSearchDto.getCriteria().containsKey(
				CommonSearchDto.END_DATE)
				&& !commonSearchDto.getCriteria().containsKey(
						CommonSearchDto.START_DATE)) {
			criteria.add(Restrictions.le("docDate", commonSearchDto
					.getCriteria().get(CommonSearchDto.END_DATE)));

		}

		// Case Search By Date Rang in Start Date and End Date
		else if (commonSearchDto.getCriteria().containsKey(
				CommonSearchDto.START_DATE)
				&& commonSearchDto.getCriteria().containsKey(
						CommonSearchDto.END_DATE)) {
			criteria.add(
					Restrictions.ge("docDate", commonSearchDto.getCriteria()
							.get(CommonSearchDto.START_DATE))).add(
					Restrictions.le("docDate", commonSearchDto.getCriteria()
							.get(CommonSearchDto.END_DATE)));
		}
		return criteria.setFetchMode("quotationDetails", FetchMode.JOIN).setFetchMode("customer", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Quotation> findQuotationByCustomer(Customer customer) {
		return sessionFactoy.getCurrentSession()
				.createCriteria(Quotation.class)
				.add(Restrictions.eq("customer",customer))
				.list();
	}

	
}
