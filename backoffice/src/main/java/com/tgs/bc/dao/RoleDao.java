package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.Permission;
import com.tgs.bc.model.Role;

public interface RoleDao   {

	public List<Role> findByCriteria (Role role);

	public List<Permission> findPermissionByRole(Role role);
}
