package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.Position;

@Repository
public class PositionDaoImpl implements PositionDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Position> findByCriteria(Position position) {
		
		return sessionFactory.getCurrentSession()
				.createCriteria(Position.class)
				.add(Restrictions.like("name",position.getName(),MatchMode.ANYWHERE))
				.list();
		
	}
	

	

	
}
