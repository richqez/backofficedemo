package com.tgs.bc.dao;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.model.CustomerPayment;
import com.tgs.bc.model.TaxReceipt;

public interface CustomerPaymentDao extends Serializable {
 
	public List<CustomerPayment> findByCriteria(CustomerPayment customerPayment);
	
}
