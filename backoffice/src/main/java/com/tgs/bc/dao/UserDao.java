package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.User;
import com.tgs.bc.model.UserRoles;

public interface UserDao {

	public List<User> findByCriteria (User user);
	
	public User login (User user);
	
	public List<UserRoles> findByCriteriaRole(User user);
		
	
	
}
