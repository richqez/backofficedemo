package com.tgs.bc.dao;

import java.util.List;





import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.TaxReceipt;

public interface TaxReceiptDao {
	
	public List<TaxReceipt> findByCriteria(CommonSearchDto<TaxReceipt> commonSearchDto);
	
	public List<TaxReceipt> findByInvoice(Invoice invoice);
	
	public List<InvoiceDetail> findInvoiceDetailByListOfInvoice(List<Invoice> listOfInvoice);
	
}
