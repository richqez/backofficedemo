package com.tgs.bc.dao;

import java.io.Serializable;

public interface GenericDao<T,ID extends Serializable> {

	void save(T entity);
	
	void update(T entity);
	
	void delete(T entiti);
	
	
}
