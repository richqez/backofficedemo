
package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.PurchaseOrder;

public interface PurchaseOrderDao  {

	public List<PurchaseOrder> findByCriteria (CommonSearchDto<PurchaseOrder> commonSearchDto);
	
	public List<PurchaseOrder> findAllPurchaseOrderAndDetail();
	
}
