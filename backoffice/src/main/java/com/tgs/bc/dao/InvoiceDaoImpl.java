package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.PurchaseOrder;

@Repository
public class InvoiceDaoImpl implements InvoiceDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Invoice> findByCriteria(CommonSearchDto<Invoice> commonSearchDto) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Invoice.class);

		if (commonSearchDto.getObject() != null) {
			if (commonSearchDto.getObject().getId() != 0) {
				criteria.add(Restrictions.eq("id", commonSearchDto.getObject()
						.getId()));
			} else if (commonSearchDto.getObject().getInvoiceNumber() != ""
					&& commonSearchDto.getObject().getInvoiceNumber() != null) {
				criteria.add(Restrictions.like("invoiceNumber", commonSearchDto
						.getObject().getInvoiceNumber(), MatchMode.ANYWHERE));
			} else if (commonSearchDto.getObject().getStartDate() != null
					&& commonSearchDto.getObject().getEndDate() != null) {
				criteria.add(
						Restrictions.ge("startDate", commonSearchDto
								.getObject().getStartDate())).add(
						Restrictions.le("endDate", commonSearchDto.getObject()
								.getEndDate()));
			} else if (commonSearchDto.getObject().getStartDate() != null
					&& commonSearchDto.getObject().getEndDate() == null) {
				criteria.add(Restrictions.ge("startDate", commonSearchDto
						.getObject().getStartDate()));
			} else if (commonSearchDto.getObject().getStartDate() == null
					&& commonSearchDto.getObject().getEndDate() != null) {
				criteria.add(Restrictions.le("endDate", commonSearchDto
						.getObject().getEndDate()));
			}

		}

		return criteria
				.setFetchMode("invoiceDetails", FetchMode.JOIN)
				.setFetchMode("purchaseOrder", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFetchMode("purchaseOrder.customer", FetchMode.JOIN)
				.setFetchMode("purchaseOrder.purchaseOrderDetails",
						FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findInvoiceByPurchaseOrder(PurchaseOrder purchaseOrder) {
		return sessionFactory.openSession().createCriteria(Invoice.class)
				.add(Restrictions.eq("purchaseOrder", purchaseOrder)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Invoice> findInvoiceByCustomer(Customer customer) {
		
		/*
		 * DetachedCriteria subQuery = DetachedCriteria
		 * .forClass(PurchaseOrder.class) .add(Restrictions.eq("customer",
		 * customer)) .setProjection(Property.forName("id"));
		 */

		return sessionFactory
				.getCurrentSession()
				.createCriteria(Invoice.class, "iv")
				.add(Subqueries.propertyIn(
						"iv.purchaseOrder",
						DetachedCriteria.forClass(PurchaseOrder.class)
								.add(Restrictions.eq("customer", customer))
								.setProjection(Property.forName("id"))))
				.add(Restrictions.eqOrIsNull("iv.taxReceipt", null))
				.setFetchMode("invoiceDetails", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

	}
}
