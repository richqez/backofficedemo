package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.TaxReceipt;

@Repository
public class TaxReceiptDaoImpl implements TaxReceiptDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<TaxReceipt> findByCriteria(
			CommonSearchDto<TaxReceipt> commonSearchDto) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				TaxReceipt.class);

		if (commonSearchDto.getObject() != null) {

			TaxReceipt receipt = commonSearchDto.getObject();

			if (!receipt.getTaxReceiptNumber().isEmpty()
					&& receipt.getTaxReceiptNumber() != null) {
				criteria.add(Restrictions.eq("taxReceiptNumber",
						receipt.getTaxReceiptNumber()));

			} else if (commonSearchDto.getCriteria().containsKey(
					CommonSearchDto.START_DATE)
					&& !commonSearchDto.getCriteria().containsKey(
							CommonSearchDto.END_DATE)) {
				criteria.add(Restrictions.ge("createdDate", commonSearchDto
						.getCriteria().get(CommonSearchDto.START_DATE)));

			} else if (commonSearchDto.getCriteria().containsKey(
					CommonSearchDto.END_DATE)
					&& !commonSearchDto.getCriteria().containsKey(
							CommonSearchDto.START_DATE)) {
				criteria.add(Restrictions.le("createdDate", commonSearchDto
						.getCriteria().get(CommonSearchDto.END_DATE)));

			} else if (commonSearchDto.getCriteria().containsKey(
					CommonSearchDto.START_DATE)
					&& commonSearchDto.getCriteria().containsKey(
							CommonSearchDto.END_DATE)) {
				criteria.add(
						Restrictions.ge("createdDate", commonSearchDto
								.getCriteria().get(CommonSearchDto.START_DATE)))
						.add(Restrictions.le("createdDate", commonSearchDto
								.getCriteria().get(CommonSearchDto.END_DATE)));

			}
		}

		return criteria
				.setFetchMode("taxReceiptDetails", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFetchMode("invoices", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFetchMode("invoices.purchaseOrder", FetchMode.JOIN)
				.setFetchMode("invoices.purchaseOrder.customer", FetchMode.JOIN)
				.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaxReceipt> findByInvoice(Invoice invoice) {

		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(TaxReceipt.class)
				.add(Restrictions.eq("invoice", invoice));

		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<InvoiceDetail> findInvoiceDetailByListOfInvoice(
			List<Invoice> listOfInvoice) {

		return sessionFactory.getCurrentSession()
				.createCriteria(InvoiceDetail.class)
				.add(Restrictions.in("invoice", listOfInvoice)).list();
	}

}
