package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.Menu;

@Repository
@Scope("singleton")
public class MenuDaoImpl implements MenuDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Menu> getAllMenuNotLv3() {
		return sessionFactory.getCurrentSession().createCriteria(Menu.class)
				.add(Restrictions.not(Restrictions.eq("level", 3))).list();
	}


	@SuppressWarnings("unchecked")
	public List<Menu> getAll() {
		return sessionFactory.getCurrentSession()
				.createCriteria(Menu.class)
				.setFetchMode("function",FetchMode.JOIN)
				.list();
	}
	

}
