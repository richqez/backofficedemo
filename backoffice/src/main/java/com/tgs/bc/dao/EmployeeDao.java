package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.EmployeeInfo;

public interface EmployeeDao  {

	
	public List<EmployeeInfo> findByCriteria (EmployeeInfo employeeInfo);
	
}
