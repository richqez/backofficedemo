package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Bill;
import com.tgs.bc.model.Invoice;

public interface BillDao {

	public List<Bill> findByCriteria(Bill bill);
	public List<Invoice> findInvoiceByBill(Bill	bill);
	public List<Bill> findByCriteria (CommonSearchDto<Bill> commonSearchDto);
}
