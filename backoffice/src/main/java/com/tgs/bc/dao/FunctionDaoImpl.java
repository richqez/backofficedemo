package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.Function;
import com.tgs.bc.model.Menu;

@Repository
@Scope("singleton")
public class FunctionDaoImpl implements FunctionDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Function> findByCriteria(Function function) {
		return sessionFactory
				.getCurrentSession()
				.createCriteria(Function.class)
				.add(Restrictions.like("name", function.getName(),
						MatchMode.ANYWHERE)).list();
	}

	@SuppressWarnings("unchecked")
	public List<Function> getAllNotInMenu() {
		return sessionFactory
				.getCurrentSession()
				.createCriteria(Function.class)
				.add(Subqueries.propertyNotIn(
						"id",
						DetachedCriteria.forClass(Menu.class).setProjection(
								Property.forName("function.id"))))
				.setResultTransformer(Criteria.ROOT_ENTITY).list();

	}

}
