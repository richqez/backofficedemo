package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.Customer;


public interface CustomerDao  {

	public List<Customer> findByCriteria(Customer customer);
	public List<Customer> findByCriteriaById(Customer customer);
}
