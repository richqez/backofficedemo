package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.Customer;

@Repository
@Scope("singleton")
public class CustomerDaoImpl implements CustomerDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Customer> findByCriteria(Customer customer) {
		
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Customer.class);
		
		if(customer.getName() != null  && customer.getName() != "" ){
			criteria.add(Restrictions.like("name",customer.getName(),MatchMode.ANYWHERE))
					.list();
		}
		
		if (customer.getCompanyName() != null && customer.getCompanyName() != "") {
			criteria.add(Restrictions.like("companyName",customer.getCompanyName(),MatchMode.ANYWHERE)).list();
		}
		
		return  criteria.addOrder(Order.asc("customerCode")).list(); 
	}

	@SuppressWarnings("unchecked")
	public List<Customer> findByCriteriaById(Customer customer) {
		return sessionFactory.getCurrentSession().createCriteria(
				Customer.class).add(Restrictions.eq("customer.id", customer.getId()))
				.list();
		
		
	}

	

}
