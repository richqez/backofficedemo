package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.User;
import com.tgs.bc.model.UserRoles;

@Repository
@Scope("singleton")
public class UserDaoImpl implements UserDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<User> findByCriteria(User user) {
		return sessionFactory
				.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.like("userName", user.getUserName(),
						MatchMode.ANYWHERE))
				.setFetchMode("userRoleses", FetchMode.DEFAULT)
				.setFetchMode("employeeInfo", FetchMode.JOIN)
				.list();

	}

	@SuppressWarnings("unchecked")
	public List<UserRoles> findByCriteriaRole(User user) {

		return sessionFactory.getCurrentSession()
				.createCriteria(UserRoles.class)
				.add(Restrictions.eq("user.id", user.getId()))
				.setFetchMode("role", FetchMode.JOIN).list();
	}

	public User login(User user) {
		return (User) sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.eq("userName", user.getUserName()))
				.add(Restrictions.eq("password", user.getPassword()))
				.uniqueResult();
	}

}
