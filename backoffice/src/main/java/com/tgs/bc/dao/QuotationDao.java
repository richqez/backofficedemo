package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Quotation;
import com.tgs.bc.model.QuotationDetail;


public interface QuotationDao {

	public List<Quotation> findByCriteria (Quotation quotation);
	
	public List<QuotationDetail> findQuotationDetailByQuotation(Quotation quotation);
	
	public List<Quotation> findByCriteria (CommonSearchDto<Quotation> commonSearchDto);

	public List<Quotation> findQuotationByCustomer(Customer customer);
}
