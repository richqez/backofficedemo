package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Bill;
import com.tgs.bc.model.Invoice;

@Repository
@Scope("singleton")
public class BillDaoImpl implements BillDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Bill> findByCriteria(Bill bill) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Bill.class);

		if (bill.getBillNumber() != null && bill.getBillNumber() != "") {
			criteria.add(Restrictions.like("billNumber", bill.getBillNumber(),
					MatchMode.ANYWHERE));
		}

		return criteria.setFetchMode("invoices", FetchMode.DEFAULT).list();
	}

	@SuppressWarnings("unchecked")
	public List<Invoice> findInvoiceByBill(Bill bill) {

		return sessionFactory.getCurrentSession().createCriteria(Invoice.class)
				.add(Restrictions.eq("bill", bill)).addOrder(Order.asc("id"))
				.setFetchMode("invoice", FetchMode.JOIN).list();
	}

	@SuppressWarnings("unchecked")
	public List<Bill> findByCriteria(CommonSearchDto<Bill> commonSearchDto) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Bill.class);

		if (commonSearchDto.getObject().getBillNumber() != ""
				&& commonSearchDto.getObject().getBillNumber() != null) {
			criteria.add(Restrictions.eq("billNumber", commonSearchDto
					.getObject().getBillNumber()));
		} else if (commonSearchDto.getCriteria() != null) {

			// Case Search By Start Date
		}
		if (commonSearchDto.getCriteria().containsKey(
				CommonSearchDto.START_DATE)
				&& !commonSearchDto.getCriteria().containsKey(
						CommonSearchDto.END_DATE)) {
			criteria.add(Restrictions.ge("billDate", commonSearchDto
					.getCriteria().get(CommonSearchDto.START_DATE)));
		}

		// Case Search By End Date
		else if (commonSearchDto.getCriteria().containsKey(
				CommonSearchDto.END_DATE)
				&& !commonSearchDto.getCriteria().containsKey(
						CommonSearchDto.START_DATE)) {
			criteria.add(Restrictions.le("billDate", commonSearchDto
					.getCriteria().get(CommonSearchDto.END_DATE)));

		}

		// Case Search By Date Rang in Start Date and End Date
		else if (commonSearchDto.getCriteria().containsKey(
				CommonSearchDto.START_DATE)
				&& commonSearchDto.getCriteria().containsKey(
						CommonSearchDto.END_DATE)) {
			criteria.add(
					Restrictions.ge("billDate", commonSearchDto.getCriteria()
							.get(CommonSearchDto.START_DATE))).add(
					Restrictions.le("billDate", commonSearchDto.getCriteria()
							.get(CommonSearchDto.END_DATE)));

		}

		return criteria.setFetchMode("invoices", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
	}

}
