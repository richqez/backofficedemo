/**
 * 
 */
package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.PurchaseOrder;

/**
 * @author Taiy
 *
 */
public interface InvoiceDao  {
	
	public List<Invoice> findByCriteria (CommonSearchDto<Invoice> commonSearchDto);
	
	public List<Invoice> findInvoiceByPurchaseOrder(PurchaseOrder purchaseOrder);
	
	public List<Invoice> findInvoiceByCustomer(Customer customer);

	
}
