package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.Function;


public interface FunctionDao{
	
	public List<Function> findByCriteria (Function function);
	
	public List<Function> getAllNotInMenu();
}
