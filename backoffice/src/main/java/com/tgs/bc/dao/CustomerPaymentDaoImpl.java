package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.CustomerPayment;


@Repository
@Scope("singleton")
public class CustomerPaymentDaoImpl implements CustomerPaymentDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6536518943954456383L;

	@Autowired
	SessionFactory sessionFactory;


	@SuppressWarnings("unchecked")
	public List<CustomerPayment> findByCriteria(CustomerPayment customerPayment) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				CustomerPayment.class);

			criteria.add(Restrictions.like("customerPaymentName", customerPayment.getCustomerPaymentName()
					, MatchMode.ANYWHERE)).setFetchMode("taxReceipt", FetchMode.JOIN).list();
					
		return criteria.list();

	}
}
