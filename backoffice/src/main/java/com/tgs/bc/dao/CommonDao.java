package com.tgs.bc.dao;

import java.io.Serializable;
import java.util.List;

public interface CommonDao extends Serializable{

	public List<?> findAll(Class<?> clazz);
	
	public Object findById(Class<?> clazz,String id);
	
	public Object findById(Class<?> clazz,int id);
	
	public Object findByIdCode(Class<?> clazz,String id);

	public void saveAndUpdate (Object object);
	
	public void delete(Object object);
	
	public List<?> findAllOrderBy(Class<?> clazz , String id);
		
	
}
