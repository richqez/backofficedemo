package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.Position;

public interface PositionDao {

		
	public List<Position> findByCriteria (Position position);

}
