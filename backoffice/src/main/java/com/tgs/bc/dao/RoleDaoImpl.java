package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.Permission;
import com.tgs.bc.model.Role;

@Repository
@Scope("singleton")
public class RoleDaoImpl implements RoleDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<Role> findByCriteria(Role role) {

		return sessionFactory
				.getCurrentSession()
				.createCriteria(Role.class)
				.add(Restrictions.like("name", role.getName(),
						MatchMode.ANYWHERE)).list();

	}

	@SuppressWarnings("unchecked")
	public List<Permission> findPermissionByRole(Role role) {
		return sessionFactory.getCurrentSession()
				.createCriteria(Permission.class, "p")
				.add(Restrictions.eq("role", role))
				.setFetchMode("workright", FetchMode.JOIN)
				.setFetchMode("function", FetchMode.JOIN)
				.addOrder(Order.asc("function"))
				.addOrder(Order.asc("workright")).list();
	}

}
