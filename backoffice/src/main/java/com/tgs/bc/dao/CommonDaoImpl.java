/**
 * 
 */
package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @author Taiy
 *
 */

@Repository
public class CommonDaoImpl implements CommonDao {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3437848447973391795L;
	@Autowired
	SessionFactory sessionFactory;

	public void saveAndUpdate(Object object) {
	
		sessionFactory.getCurrentSession().saveOrUpdate(object);
	}

	public void delete(Object object) {
		sessionFactory.getCurrentSession().delete(object);
	}

	public List<?> findAll(Class<?> clazz) {
		return sessionFactory.getCurrentSession().createCriteria(clazz).list();
	}

	public Object findById(Class<?> clazz, int id) {
		return sessionFactory.getCurrentSession().get(clazz, id);
	}

	public Object findById(Class<?> clazz, String id) {
		return sessionFactory.getCurrentSession().get(clazz, new Integer(id));
	}
	
	

	public Object findByIdCode(Class<?> clazz, String id) {
		return sessionFactory.getCurrentSession().get(clazz, id);
	}

	
	
	public List<?> findAllOrderBy(Class<?> clazz, String id) {
		return sessionFactory.getCurrentSession().createCriteria(clazz).addOrder(Order.asc(id)).list();
	}

}
