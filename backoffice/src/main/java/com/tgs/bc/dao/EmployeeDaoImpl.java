package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.model.EmployeeInfo;

@Repository
@Scope("singleton")
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<EmployeeInfo> findByCriteria(EmployeeInfo employeeInfo) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				EmployeeInfo.class);

		if (employeeInfo.getFirstName() != null
				&& employeeInfo.getFirstName() != "") {
			criteria.add(Restrictions.like("firstName",
					employeeInfo.getFirstName(), MatchMode.ANYWHERE));
		}

		else if (employeeInfo.getLastName() != null
				&& employeeInfo.getLastName() != "") {
			criteria.add(Restrictions.like("lastName",
					employeeInfo.getLastName(), MatchMode.ANYWHERE));
		}

		return criteria.createAlias("users", "user", JoinType.LEFT_OUTER_JOIN)
				.setFetchMode("position", FetchMode.JOIN).list();

	}

}
