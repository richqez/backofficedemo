package com.tgs.bc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.PurchaseOrder;

/**
 * @author Taiy
 *
 */

@Repository
@Scope("singleton")
public class PurchaseOrderDaoImpl implements PurchaseOrderDao {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> findByCriteria(
			CommonSearchDto<PurchaseOrder> commonSearchDto) {

		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				PurchaseOrder.class);

		if (commonSearchDto.getObject().getPoNumber() != null
				&& commonSearchDto.getObject().getPoNumber() != "") {
			criteria.add(Restrictions.eq("poNumber", commonSearchDto
					.getObject().getPoNumber()));

		} else if (commonSearchDto.getCriteria() != null) {

			/*
			 * 
			 * case search by >= start date
			 */
			if (commonSearchDto.getCriteria().containsKey(
					CommonSearchDto.START_DATE)
					&& !commonSearchDto.getCriteria().containsKey(
							CommonSearchDto.END_DATE)) {
				criteria.add(Restrictions.ge("docDate", commonSearchDto
						.getCriteria().get(CommonSearchDto.START_DATE)));

			}
			/*
			 * 
			 * case search by =< end date
			 */
			else if (commonSearchDto.getCriteria().containsKey(
					CommonSearchDto.END_DATE)
					&& !commonSearchDto.getCriteria().containsKey(
							CommonSearchDto.START_DATE)) {
				criteria.add(Restrictions.le("docDate", commonSearchDto
						.getCriteria().get(CommonSearchDto.END_DATE)));

			}
			/*
			 * case search by date range date in start date and end date
			 */
			else if (commonSearchDto.getCriteria().containsKey(
					CommonSearchDto.START_DATE)
					&& commonSearchDto.getCriteria().containsKey(
							CommonSearchDto.END_DATE)) {
				criteria.add(
						Restrictions.ge("docDate", commonSearchDto
								.getCriteria().get(CommonSearchDto.START_DATE)))
						.add(Restrictions.le("docDate", commonSearchDto
								.getCriteria().get(CommonSearchDto.END_DATE)));
			}

		}

		return criteria.setFetchMode("purchaseOrderDetails", FetchMode.JOIN)
				.setFetchMode("customer", FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PurchaseOrder> findAllPurchaseOrderAndDetail() {
		return sessionFactory.getCurrentSession()
				.createCriteria(PurchaseOrder.class)
				.setFetchMode("purchaseOrderDetails",FetchMode.JOIN)
				.setFetchMode("customer",FetchMode.JOIN)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.list();
	}
}
