package com.tgs.bc.dao;

import java.util.List;

import com.tgs.bc.model.Menu;

public interface MenuDao  {

	public List<Menu> getAllMenuNotLv3();
	
	public List<Menu> getAll();
}
