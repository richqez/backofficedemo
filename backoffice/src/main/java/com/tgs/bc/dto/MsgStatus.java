package com.tgs.bc.dto;
/**
 * @author Taiy
 *
 * For transfer result from  service layer to controller
 * You can transfer everything for validation bah bah .
 *
 */
public class MsgStatus {

	String statusStr;
	Boolean status;
	String message;
	Object value;
	
	
	public MsgStatus(String statusStr, Boolean status, String message,
			Object value) {
		super();
		this.statusStr = statusStr;
		this.status = status;
		this.message = message;
		this.value = value;
	}
	
	
	public MsgStatus() {
	}

	public String getStatusStr() {
		return statusStr;
	}
	public void setStatusStr(String statusStr) {
		this.statusStr = statusStr;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	public static class Builder{
		String statusStr;
		Boolean status;
		String message;
		Object value;
		
		public Builder (final  String statusStr , final Boolean stBoolean){
			this.statusStr = statusStr ;
			this.status = stBoolean;
		}
		
		public MsgStatus create(){
			return new MsgStatus(this.statusStr,this.status,this.message,this.value);
		}

		public Builder setStatusStr(String statusStr) {
			this.statusStr = statusStr;
			return this;
		}

		public Builder setStatus(Boolean status) {
			this.status = status;
			return this;
		}

		public Builder setMessage(String message) {
			this.message = message;
			return this;
		}

		public Builder setValue(Object value) {
			this.value = value;
			return this;
		}

		
	}

}
