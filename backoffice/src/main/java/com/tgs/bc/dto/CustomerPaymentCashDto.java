package com.tgs.bc.dto;

import java.io.Serializable;
import java.util.Date;

public class CustomerPaymentCashDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2610057740753796193L;

	public Double amount;
	public Date accountDate;
	public Double withHoldingTax;
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getWithHoldingTax() {
		return withHoldingTax;
	}
	public void setWithHoldingTax(Double withHoldingTax) {
		this.withHoldingTax = withHoldingTax;
	}
	public Date getAccountDate() {
		return accountDate;
	}
	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
	}
	
	
	
}
