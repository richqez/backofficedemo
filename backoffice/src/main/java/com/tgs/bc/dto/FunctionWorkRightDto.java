package com.tgs.bc.dto;

import java.util.List;

import com.tgs.bc.model.Function;
import com.tgs.bc.model.Workright;

/**
 * @author Taiy
 *
 * Data Transfer object for role screen
 * 
 * Y
 *
 *
 */
public class FunctionWorkRightDto {

	
	Function function ; 
	
	List<Workright> workrights;
	


	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public List<Workright> getWorkrights() {
		return workrights;
	}

	public void setWorkrights(List<Workright> workrights) {
		this.workrights = workrights;
	}









	
	
}
