package com.tgs.bc.dto;

import java.io.Serializable;
import java.util.Date;

public class CustomerPaymentTransferDto implements  Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3138146057052003821L;

	
	public Double amount;
	public Date accountDate;
	public Double withHoldingTax;
	public Double fee;
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getWithHoldingTax() {
		return withHoldingTax;
	}
	public void setWithHoldingTax(Double withHoldingTax) {
		this.withHoldingTax = withHoldingTax;
	}
	public Double getFee() {
		return fee;
	}
	public void setFee(Double fee) {
		this.fee = fee;
	}
	public Date getAccountDate() {
		return accountDate;
	}
	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
	}
	
	
	
}
