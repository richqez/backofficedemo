package com.tgs.bc.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;


/**
 * @author Taiy
 *
 * @param <T>  => Domain Class
 * 
 * for transfer data to method findByCriteria 
 * 
 */
public class CommonSearchDto<T> implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -486729711776953976L;
	public static final String  END_DATE = "END_DATE";
	public static final String  START_DATE = "START_DATE";

	
	
	T object ;
	
	Hashtable<String,Date> criteria ;
	

	public Hashtable<String,Date> getCriteria() {
		return criteria;
	}

	public void setCriteria(Hashtable<String,Date> criteria) {
		this.criteria = criteria;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}
	
}
