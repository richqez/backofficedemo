package com.tgs.bc.dto;

import java.io.Serializable;
import java.util.Date;

public class CustomerPaymentCheckDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6260945012296304428L;

	
	public Double amount;
	public Date accountDate;
	public Double withHoldingTax;
	public String bank;
	public Integer chcekNo;
	public Date checkDate;
	public Date depositDate;
	public Double depositBalance;
	public Integer depositAccount;
	
	
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Double getWithHoldingTax() {
		return withHoldingTax;
	}
	public void setWithHoldingTax(Double withHoldingTax) {
		this.withHoldingTax = withHoldingTax;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public Integer getChcekNo() {
		return chcekNo;
	}
	public void setChcekNo(Integer chcekNo) {
		this.chcekNo = chcekNo;
	}
	public Date getCheckDate() {
		return checkDate;
	}
	public void setCheckDate(Date checkDate) {
		this.checkDate = checkDate;
	}
	public Date getDepositDate() {
		return depositDate;
	}
	public void setDepositDate(Date depositDate) {
		this.depositDate = depositDate;
	}
	public Double getDepositBalance() {
		return depositBalance;
	}
	public void setDepositBalance(Double depositBalance) {
		this.depositBalance = depositBalance;
	}
	public Integer getDepositAccount() {
		return depositAccount;
	}
	public void setDepositAccount(Integer depositAccount) {
		this.depositAccount = depositAccount;
	}
	public Date getAccountDate() {
		return accountDate;
	}
	public void setAccountDate(Date accountDate) {
		this.accountDate = accountDate;
	}
	
	
}