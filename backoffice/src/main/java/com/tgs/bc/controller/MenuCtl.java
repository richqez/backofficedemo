package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.model.Function;
import com.tgs.bc.model.Menu;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.FunctionSv;
import com.tgs.bc.service.MenuSv;

@Controller
@Scope("request")
public class MenuCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1226115865543583504L;

	Menu menu;

	List<Menu> listOfMenu;

	List<Function> listOfFunction;

	List<Menu> listOfMenuNotLv3;

	TreeNode menuRoot;

	@Autowired
	CommonSv commonSv;

	@Autowired
	FunctionSv functionSv;

	@Autowired
	LoginCtl	loginCtl;
	
	Menu rootMenu = new Menu();

	@Autowired
	MenuSv menuSv;

	@PostConstruct
	public void init() {
		menu = new Menu();
		listOfMenu = (List<Menu>) menuSv.getAll();
		listOfFunction = functionSv.getAllNotInMenu();
		listOfMenuNotLv3 = menuSv.getAllMenuNotLv3();
		rootMenu.setName("root");
		rootMenu.setId(-99);
		loadMenuTree();
	}

	public void loadMenuTree() {

		int menuLv1Id = 0;
		int menuLv2Id = 0;

		DefaultTreeNode nodeLv1 = null;
		DefaultTreeNode nodeLv2;

		menuRoot = new DefaultTreeNode(rootMenu, null);

		for (Menu menuLv1 : listOfMenu) {
			if (menuLv1.getMenu() == null) {
				nodeLv1 = new DefaultTreeNode(menuLv1, menuRoot);
				menuLv1Id = menuLv1.getId();
				for (Menu menuLv2 : listOfMenu) {
					if (!(menuLv2.getMenu() == null)) {
						if (menuLv2.getMenu().getId() == menuLv1Id) {
							nodeLv2 = new DefaultTreeNode(menuLv2, nodeLv1);
							menuLv2Id = menuLv2.getId();
							for (Menu menuLv3 : listOfMenu) {
								if (!(menuLv3.getMenu() == null)) {
									if (menuLv3.getMenu().getId() == menuLv2Id) {
										new DefaultTreeNode(menuLv3, nodeLv2);
									}
								}
							}
						}
					}

				}

			}

		}

	}

	public void save() {

		if (menu.getId() != 0) {
			menu.setUpdatedBy(loginCtl.getUser().getUserName());
			menu.setUpdatedDate(new Date());
		}else {
			menu.setCreatedBy(loginCtl.getUser().getUserName());
			menu.setCreatedDate(new Date());
		}
		
		
		if (menu.getMenu() != null &&  menu.getMenu().getId() != - 99 ) {
			menu.setLevel(menu.getMenu().getLevel() + 1);
		}

		if (menu.getMenu() != null) {
			if (menu.getMenu().getId() == -99) {
				menu.setLevel(1);
				menu.setMenu(null);
			}
		}

		commonSv.saveAndUpdate(menu);
		init();
	}

	public void update(Menu menu) {
		setMenu(menu);
	}

	public void delete(Menu menu) {
		commonSv.delete(menu);
		init();
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public List<Menu> getListOfMenu() {
		return listOfMenu;
	}

	public void setListOfMenu(List<Menu> listOfMenu) {
		this.listOfMenu = listOfMenu;
	}

	public List<Function> getListOfFunction() {
		return listOfFunction;
	}

	public void setListOfFunction(List<Function> listOfFunction) {
		this.listOfFunction = listOfFunction;
	}

	public TreeNode getMenuRoot() {
		return menuRoot;
	}

	public void setMenuRoot(TreeNode menuRoot) {
		this.menuRoot = menuRoot;
	}

	public List<Menu> getListOfMenuNotLv3() {
		return listOfMenuNotLv3;
	}

	public void setListOfMenuNotLv3(List<Menu> listOfMenuNotLv3) {
		this.listOfMenuNotLv3 = listOfMenuNotLv3;
	}

	public Menu getRootMenu() {
		return rootMenu;
	}

	public void setRootMenu(Menu rootMenu) {
		this.rootMenu = rootMenu;
	}

}
