package com.tgs.bc.controller;

import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.model.Menu;
import com.tgs.bc.service.MenuSv;

@Controller
@Scope("request")
public class MenuGenerator {

	List<Menu> listOfmenu;

	MenuModel menuModel;

	@Autowired
	MenuSv menuSv;

	@PostConstruct
	public void init(){
		
		menuModel = new DefaultMenuModel();
   
		
		listOfmenu = menuSv.getAll();
		
		DefaultSubMenu sub ;
		int subId = 0 ;
		
		for (Menu m : listOfmenu) {
			
			if (m.getFunction() != null) {
				if (m.getFunction().getUrl().equals("#")) {
					
					sub = new DefaultSubMenu(m.getName());
					
					for (Menu menu : listOfmenu) {
						
						if (menu.getMenu()!=null) {
							DefaultMenuItem defaultMenuItem = new DefaultMenuItem(menu.getName());
							defaultMenuItem.setUrl(menu.getFunction().getUrl());
							sub.addElement(defaultMenuItem);
						}
						
					}
					
					menuModel.addElement(sub);

				}
				
			}
			
			
		}
		
		
		
		
	}

	public MenuModel getMenuModel() {
		return menuModel;
	}

	public void setMenuModel(MenuModel menuModel) {
		this.menuModel = menuModel;
	}

}
