package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.CustomerPaymentCashDto;
import com.tgs.bc.dto.CustomerPaymentCheckDto;
import com.tgs.bc.dto.CustomerPaymentTransferDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.CustomerPayment;
import com.tgs.bc.model.TaxReceipt;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.CustomerPaymentSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class CustomerPaymentCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7167110314238987676L;

	@Autowired
	CommonSv commonSv;

	@Autowired
	CustomerPaymentSv customerPaymentSv;

	@Autowired
	LoginCtl loginCtl;

	CustomerPayment customerPayment;
	
	CustomerPaymentCashDto customerPaymentCashDto; // จำลองจาก โมเดล CustomerPayment โดย เคส เงินสด (cash)

	CustomerPaymentTransferDto customerPaymentTransferDto; // จำลองจาก โมเดล CustomerPayment โดย เคส โอน (transfer)

	CustomerPaymentCheckDto customerPaymentCheckDto; // จำลองจาก โมเดล CustomerPayment โดย เคส เช็ค (check)

	CustomerPayment sCustomerPayment;  // ใช้ในการค้นหา CustomerPayment

	List<CustomerPayment> listOfCustomerPayment; // list CustomerPayment ที่ได้จากการค้นหา

	List<TaxReceipt> listOfTaxReceipt; // list TaxReceipt ทั้งหมดห

	List<TaxReceipt> filteredTaxReceipt;  // ตัวกรอง TaxReceipt ใช้ร่วมกับ Data table 

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {

		listOfTaxReceipt = (List<TaxReceipt>) commonSv
				.findAll(TaxReceipt.class);
		sCustomerPayment = new CustomerPayment();
		customerPayment = new CustomerPayment();
		listOfCustomerPayment = new ArrayList<CustomerPayment>();
		customerPaymentCashDto = new CustomerPaymentCashDto();
		customerPaymentTransferDto = new CustomerPaymentTransferDto();
		customerPaymentCheckDto = new CustomerPaymentCheckDto();

	}

	public void save() {
		
		if (customerPayment.getTaxReceipt() != null
				&& !customerPayment.getCustomerPaymentName().equals("")) {

			if (customerPaymentSv.CheckTaxReceipt(customerPayment,
					customerPaymentCashDto, customerPaymentTransferDto,
					customerPaymentCheckDto, customerPayment.getTaxReceipt())) {

				if (customerPayment.getId() != 0) {

					customerPayment.setUpdatedBy(loginCtl.getUser()
							.getUserName());
					customerPayment.setUpdatedDate(new Date());

				} else {

					customerPayment.setCreatedBy(loginCtl.getUser()
							.getUserName());
					customerPayment.setCreatedDate(new Date());

					customerPayment.setUpdatedBy(loginCtl.getUser()
							.getUserName());
					customerPayment.setUpdatedDate(new Date());
				}

				MsgStatus msgStatus = customerPaymentSv.saveCustomerPayment(
						customerPayment, customerPaymentCashDto,
						customerPaymentTransferDto, customerPaymentCheckDto);
				JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
						.show();
				init();

			} 

		}else {
			FacesContext
			.getCurrentInstance()
			.addMessage(
					null,
					new FacesMessage(
							FacesMessage.SEVERITY_WARN,
							"กรุณาเลือกใบกำกับภาษีและเลือกประเภทการชำระเงิน",
							""));
}

	}

	public void update(CustomerPayment customerPayment) {

		this.customerPayment = customerPayment;

		switch (customerPayment.getCustomerPaymentName()) {
		case "cash":
			
			customerPaymentCheckDto = new CustomerPaymentCheckDto();
			customerPaymentTransferDto = new CustomerPaymentTransferDto();
			break;

		case "transfer":
			customerPaymentCashDto = new CustomerPaymentCashDto();
			customerPaymentCheckDto = new CustomerPaymentCheckDto();

		case "check":
			customerPaymentCashDto = new CustomerPaymentCashDto();
			customerPaymentTransferDto = new CustomerPaymentTransferDto();
			break;
		}

		customerPaymentSv.updateCustomerPayment(customerPayment,
				customerPaymentCashDto, customerPaymentTransferDto,
				customerPaymentCheckDto);

	}

	public void delete(CustomerPayment customerPayment) {
		commonSv.delete(customerPayment);
		init();
	}

	public void search() {

		listOfCustomerPayment = customerPaymentSv
				.findByCriteria(sCustomerPayment);
		System.out.print(sCustomerPayment.getCustomerPaymentName());
		sCustomerPayment = new CustomerPayment();
	}

	public void seleteTaxReceipt(TaxReceipt taxReceipt) {

		getCustomerPayment().setTaxReceipt(taxReceipt);
	}

	// Getter Setter
	public CustomerPayment getCustomerPayment() {
		return customerPayment;
	}

	public void setCustomerPayment(CustomerPayment customerPayment) {
		this.customerPayment = customerPayment;
	}

	public List<TaxReceipt> getListOfTaxReceipt() {
		return listOfTaxReceipt;
	}

	public void setListOfTaxReceipt(List<TaxReceipt> listOfTaxReceipt) {
		this.listOfTaxReceipt = listOfTaxReceipt;
	}

	public CustomerPaymentCashDto getCustomerPaymentCash() {
		return customerPaymentCashDto;
	}

	public void setCustomerPaymentCash(
			CustomerPaymentCashDto customerPaymentCash) {
		this.customerPaymentCashDto = customerPaymentCash;
	}

	public CustomerPaymentTransferDto getCustomerPaymentTransfer() {
		return customerPaymentTransferDto;
	}

	public void setCustomerPaymentTransfer(
			CustomerPaymentTransferDto customerPaymentTransfer) {
		this.customerPaymentTransferDto = customerPaymentTransfer;
	}

	public CustomerPaymentCheckDto getCustomerPaymentCheck() {
		return customerPaymentCheckDto;
	}

	public void setCustomerPaymentCheck(
			CustomerPaymentCheckDto customerPaymentCheck) {
		this.customerPaymentCheckDto = customerPaymentCheck;
	}

	public List<TaxReceipt> getFilteredTaxReceipt() {
		return filteredTaxReceipt;
	}

	public void setFilteredTaxReceipt(List<TaxReceipt> filteredTaxReceipt) {
		this.filteredTaxReceipt = filteredTaxReceipt;
	}

	public CustomerPayment getsCustomerPayment() {
		return sCustomerPayment;
	}

	public void setsCustomerPayment(CustomerPayment sCustomerPayment) {
		this.sCustomerPayment = sCustomerPayment;
	}

	public List<CustomerPayment> getListOfCustomerPayment() {
		return listOfCustomerPayment;
	}

	public void setListOfCustomerPayment(
			List<CustomerPayment> listOfCustomerPayment) {
		this.listOfCustomerPayment = listOfCustomerPayment;
	}

}
