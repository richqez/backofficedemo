package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Position;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.PositionSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class PositionCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7267845364394054342L;

	// Variable for map bundle
	Position position;

	// Variable for search method
	Position sPosition;

	// Variable for map DataTable
	List<Position> listOfPosition;

	// Inject Service
	@Autowired
	CommonSv commonSv;

	@Autowired
	PositionSv positionSv;

	@Autowired
	LoginCtl loginCtl;

	@PostConstruct
	public void init() {
		position = new Position();
		sPosition = new Position();
		listOfPosition = new ArrayList<Position>();
	}

	public void save() {

		// case update entity
		if (position.getId() != 0) {

			position.setUpdatedDate(new Date());
			position.setCreatedBy(loginCtl.getUser().getUserName());
		}
		// case create entity
		else {

			position.setCreatedDate(new Date());
			position.setCreatedBy(loginCtl.getUser().getUserName());

			position.setUpdatedDate(new Date());
			position.setUpdatedBy(loginCtl.getUser().getUserName());

		}

		MsgStatus msgStatus = commonSv.saveAndUpdate(position);

		if (msgStatus.getStatus()) {

			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		} else {

			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setFatalMessage().show();
		}

		init();
	}

	public void delete(Position position) {

		MsgStatus msgStatus = commonSv.delete(position);

		if (msgStatus.getStatus()) {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		}
		else{
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
			.setFatalMessage().show();
		}

		init();
	}

	public void update(Position position) {
		this.position = position;
	}

	public void search() {
		listOfPosition = positionSv.findByCriteria(sPosition);
		sPosition = new Position();
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public List<Position> getListOfPosition() {
		return listOfPosition;
	}

	public void setListOfPosition(List<Position> listOfPosition) {
		this.listOfPosition = listOfPosition;
	}

	public Position getsPosition() {
		return sPosition;
	}

	public void setsPosition(Position sPosition) {
		this.sPosition = sPosition;
	}

}
