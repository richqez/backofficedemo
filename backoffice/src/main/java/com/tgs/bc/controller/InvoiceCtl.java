package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.PurchaseOrder;
import com.tgs.bc.model.PurchaseOrderDetail;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.InvoiceSv;
import com.tgs.bc.service.PurchaseOrderSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
public class InvoiceCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5006243713119217202L;

	List<Invoice> listOfInvoice; // variable for show in data table
	List<PurchaseOrder> listOfPurchaseOrder; // variable for search and select
												// for search
												// purchaseOrderDetail
	List<PurchaseOrder> listOfPurchaseOrderFilltered;

	List<PurchaseOrderDetail> listOfSelectedOrderDetail;

	PurchaseOrder purchaseOrder;

	List<InvoiceDetail> listOfInvoiceDetail; // store data for save and show in
												// view
	Invoice sInvoice; // for search only
	PurchaseOrder sPurchaseOrder; // for search only
	Boolean isSearchDateRangInvoice, isSearchDateRangPurchaseOrder;
	Invoice invoice;

	Date startDate;
	Date endDate;

	boolean selectPurchaseOrder = false;

	CommonSearchDto<Invoice> commonSearchDto;

	@Autowired
	InvoiceSv invoiceSv;

	@Autowired
	PurchaseOrderSv purchaseOrderSv;

	@Autowired
	CommonSv commonSv;

	@PostConstruct
	public void init() {

		invoice = new Invoice();
		sInvoice = new Invoice();
		sPurchaseOrder = new PurchaseOrder();

		listOfInvoice = new ArrayList<Invoice>();
		listOfPurchaseOrder = purchaseOrderSv.findAllPurchaseOrderAndDetail();
		listOfSelectedOrderDetail = new ArrayList<PurchaseOrderDetail>();
		purchaseOrder = new PurchaseOrder();
		listOfInvoiceDetail = new ArrayList<InvoiceDetail>();

		isSearchDateRangInvoice = false;
		isSearchDateRangPurchaseOrder = false;

		commonSearchDto = new CommonSearchDto<Invoice>();
		selectPurchaseOrder = false;
		
		startDate = null;
		endDate = null;

	}

	public void deleteInvoice(Invoice invoice) {
		MsgStatus msgStatus = commonSv.delete(invoice);
		JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();
	}

	public void saveInvoice() {

		if (!listOfInvoiceDetail.isEmpty()) {
			MsgStatus msgStatus = invoiceSv.saveInvoiceAndInvoiceDetail(
					invoice, listOfInvoiceDetail, invoice.getPurchaseOrder());

			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();

			if (msgStatus.getStatus()) {
				init();
			}

		} else {

			JsfMessagesUtils.getFacessMessage()
					.setHeadMessage("คุณจำเป็นต้องเพิ่มรายการ invoice")
					.setFatalMessage().show();

		}

	}

	public void validationCustomerSelected() {
		if (!selectPurchaseOrder) {
			JsfMessagesUtils
					.getFacessMessage()
					.setHeadMessage(
							"กรุณาเลือก PurchaseOrder ก่อทำการเพิ่มรายการ Invoice")
					.setFatalMessage().show();
		}
	}

	public void updateInvoice(Invoice invoice) {
		init();
		setInvoice(invoice);
		invoice.getInvoiceDetails()
				.forEach(ivd -> listOfInvoiceDetail.add(ivd));
		
		selectPurchaseOrder = true;

	}

	public void selectPurchaseOrderDetail() {

		listOfSelectedOrderDetail.forEach(pod -> {

			InvoiceDetail invoiceDetail = new InvoiceDetail();
			invoiceDetail.setDescription(pod.getDescription());
			invoiceDetail.setAmount(pod.getAmount());
			invoiceDetail.setUnitPrice(pod.getUnitPrice());
			invoiceDetail.setUnitType(pod.getUnitType());
			invoiceDetail.setQuantity(pod.getQuantity());
			invoiceDetail.setProjectName(pod.getProjectName());
			invoiceDetail.setPeriodDescription(pod.getPeriodDescription());
			invoiceDetail.setStartDate(pod.getStartDate());
			invoiceDetail.setEndDate(pod.getEndDate());

			this.listOfInvoiceDetail.add(invoiceDetail);

		});

		invoiceSv.calculateInvoice(invoice, listOfInvoiceDetail);

	}

	public void automatedCalculateInvoice() {
		invoiceSv.calculateInvoice(invoice, listOfInvoiceDetail);
	}

	public void deleteInvoiceDetail(InvoiceDetail invoiceDetail) {
		this.listOfInvoiceDetail.remove(invoiceDetail);
	}

	public void searchInvoice() {

		commonSearchDto.setObject(sInvoice);


		listOfInvoice = invoiceSv.findByCriteria(commonSearchDto);

		commonSearchDto = new CommonSearchDto<Invoice>();
		sInvoice = new Invoice();
		startDate = null;
		endDate = null;

	}

	public void selectPurchaseOrder(PurchaseOrder purchaseOrder) {

		this.getInvoice().setPurchaseOrder(purchaseOrder);
		selectPurchaseOrder = true;
		JsfMessagesUtils.getFacessMessage()
				.setHeadMessage("คุณทำการเลือก PuchaseOrder สำเร็จ ")
				.setInfoMessage().show();

	}

	public void deleteInvoiceDetailFromList(InvoiceDetail invDetail) {
		listOfInvoiceDetail.remove(invDetail);
	}

	public List<Invoice> getListOfInvoice() {
		return listOfInvoice;
	}

	public void setListOfInvoice(List<Invoice> listOfInvoice) {
		this.listOfInvoice = listOfInvoice;
	}

	public List<PurchaseOrder> getListOfPurchaseOrder() {
		return listOfPurchaseOrder;
	}

	public void setListOfPurchaseOrder(List<PurchaseOrder> listOfPurchaseOrder) {
		this.listOfPurchaseOrder = listOfPurchaseOrder;
	}

	public List<PurchaseOrderDetail> getListOfSelectedOrderDetail() {
		return listOfSelectedOrderDetail;
	}

	public void setListOfSelectedOrderDetail(
			List<PurchaseOrderDetail> listOfSelectedOrderDetail) {
		this.listOfSelectedOrderDetail = listOfSelectedOrderDetail;
	}

	public List<InvoiceDetail> getListOfInvoiceDetail() {
		return listOfInvoiceDetail;
	}

	public void setListOfInvoiceDetail(List<InvoiceDetail> listOfInvoiceDetail) {
		this.listOfInvoiceDetail = listOfInvoiceDetail;
	}

	public Invoice getsInvoice() {
		return sInvoice;
	}

	public void setsInvoice(Invoice sInvoice) {
		this.sInvoice = sInvoice;
	}

	public PurchaseOrder getsPurchaseOrder() {
		return sPurchaseOrder;
	}

	public void setsPurchaseOrder(PurchaseOrder sPurchaseOrder) {
		this.sPurchaseOrder = sPurchaseOrder;
	}

	public Boolean getIsSearchDateRangInvoice() {
		return isSearchDateRangInvoice;
	}

	public void setIsSearchDateRangInvoice(Boolean isSearchDateRangInvoice) {
		this.isSearchDateRangInvoice = isSearchDateRangInvoice;
	}

	public Boolean getIsSearchDateRangPurchaseOrder() {
		return isSearchDateRangPurchaseOrder;
	}

	public void setIsSearchDateRangPurchaseOrder(
			Boolean isSearchDateRangPurchaseOrder) {
		this.isSearchDateRangPurchaseOrder = isSearchDateRangPurchaseOrder;
	}

	public Invoice getInvoice() {
		return invoice;
	}

	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<PurchaseOrder> getListOfPurchaseOrderFilltered() {
		return listOfPurchaseOrderFilltered;
	}

	public void setListOfPurchaseOrderFilltered(
			List<PurchaseOrder> listOfPurchaseOrderFilltered) {
		this.listOfPurchaseOrderFilltered = listOfPurchaseOrderFilltered;
	}

	public boolean isSelectPurchaseOrder() {
		return selectPurchaseOrder;
	}

	public void setSelectPurchaseOrder(boolean selectPurchaseOrder) {
		this.selectPurchaseOrder = selectPurchaseOrder;
	}

}
