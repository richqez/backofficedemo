package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.EmployeeInfo;
import com.tgs.bc.model.Role;
import com.tgs.bc.model.User;
import com.tgs.bc.model.UserRoles;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.RoleSv;
import com.tgs.bc.service.UserSv;
import com.tgs.bc.utill.JsfMessagesUtils;


@Controller
@Scope("session")
public class UserCtl implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 8551302485250118294L;

	@Autowired
	CommonSv commonSv;

	@Autowired
	UserSv userSv; 
	
	@Autowired
	RoleSv roleSv;
	
	@Autowired
	LoginCtl loginCtl;
	
	User user; 

	UserRoles userRoles; // ใช้ในการเพิ่ม user role
 
	User sUser; // ใช้ในการค้นหา User
	
	Role sRole;  // ใช้ในการค้นหา Role
	
	List<Role> listSelectRole; // list ที่ได้จาก เลือก Role

	List<User> listOfUser; // list User ที่ได้จากการค้นหา

	List<Role> listOfRole; // list Role ทั้งหมด

	List<Role> filteredRole; // ตัวกรอง Role ใช้ร่วมกับ Data table 
	
	List<EmployeeInfo> listOfEmployee; // list Employee ทั้งหมด แสดงในรูปแบบ OneMenu
	
	@SuppressWarnings({ "unchecked"})
	@PostConstruct
	public void init() {

		listOfEmployee = (List<EmployeeInfo>) commonSv
				.findAll(EmployeeInfo.class);
		sUser = new User();
		user = new User();
		sRole = new Role();
		listOfUser = new ArrayList<User>();
		listOfRole = (List<Role>) commonSv.findAll(Role.class);
		listSelectRole = new ArrayList<Role>();
	}
	

	public void save() {
				
		
		//เช็คถ้ามีไอดีของ User แสดงว่าคือเคสแก้ไข จะให้อัพเดทวันที่แก้ไข
		//แต่ถ้าไม่มี คือ เคสบันทึก ให้ใส่วันสร้างและแก้ไข
		if (user.getId() != 0) {
			user.setUpdatedBy(loginCtl.getUser().getUserName());
			user.setUpdatedDate(new Date());
		} else {

			user.setCreatedDate(new Date());
			user.setCreatedBy(loginCtl.getUser().getUserName());
			
			user.setUpdatedBy(loginCtl.getUser().getUserName());
			user.setUpdatedDate(new Date());
		}

		if (!listSelectRole.isEmpty()) {
			
			MsgStatus msgStatus = userSv.saveUserRoles(user, listSelectRole);
			
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();
			
			init();
		}else {
			JsfMessagesUtils.getFacessMessage()
			.setHeadMessage("กรุณาเลือก บทบาท")
			.setFatalMessage().show();
		}
		
			
	
	}

	public void update(User user) {
		
		listSelectRole = new ArrayList<Role>();
		
		this.user= user;
		
		if (user.getId()!=0) {
			List<UserRoles> listUserRoles = userSv.findByCriteriaRole(user);
			for (UserRoles userRoles : listUserRoles) {
				listSelectRole.add(userRoles.getRole());
				
			}
		}
		
	}
	
	//remove ค่าในListSelectRole หน้า สร้างผู้ใช้
	public void removeRole(Role role) {
		listSelectRole.remove(role);
	}
	
	public void delete(User user) {
		commonSv.delete(user);
		init();
	}

	public void search() {
		
		listOfUser = userSv.findByCriteria(sUser);
		sUser = new User();
		
	}
	
	//เช็คเงื่อนไข คือ ถ้า Role ที่รับเข้ามาห้ามซ้ำกับที่มีอยู่แล้ว
	public Boolean isList(Role role) {
		for (Role r : listSelectRole) {
			if (role.getId() == r.getId()) {
				return true;
			}
		}
		return false;
	}
	

	
	public void selectRole(Role role){
		
		
			if (!isList(role)) {
				listSelectRole.add(role);
			}else {
				FacesContext.getCurrentInstance().addMessage
							(null, new FacesMessage(FacesMessage.SEVERITY_FATAL
									,"ไม่สามารถเพิ่มบทบาทได้ เนื่องจาก บทบาทนี้ถูกเลือกแล้ว", ""));
			}
		
	
	}
	
	
	public List<Role> getListSelectRole() {
		return listSelectRole;
	}

	public void setListSelectRole(List<Role> listSelectRole) {
		this.listSelectRole = listSelectRole;
	}

	public void findRole() {
		listOfRole = roleSv.findByCriteria(sRole);
		sRole = new Role();
	}
	
	
	public List<User> getListOfUser() {
		return listOfUser;
	}

	public void setListOfUser(List<User> listOfUser) {
		this.listOfUser = listOfUser;
	}

	public List<Role> getListOfRole() {
		return listOfRole;
	}

	public void setListOfRole(List<Role> listOfRole) {
		this.listOfRole = listOfRole;
	}

	public List<EmployeeInfo> getListOfEmployee() {
		return listOfEmployee;
	}

	public void setListOfEmployee(List<EmployeeInfo> listOfEmployee) {
		this.listOfEmployee = listOfEmployee;
	}

	public Role getsRole() {
		return sRole;
	}

	public void setsRole(Role sRole) {
		this.sRole = sRole;
	}

	public User getsUser() {
		return sUser;
	}

	public void setsUser(User sUser) {
		this.sUser = sUser;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getlistOfUser() {
		return listOfUser;
	}

	public void setlistOfUser(List<User> listOfUser) {
		this.listOfUser = listOfUser;
	}

	public List<Role> getlistOfRole() {
		return listOfRole;
	}

	public void setlistOfRole(List<Role> listOfRole) {
		this.listOfRole = listOfRole;
	}

	public List<EmployeeInfo> getlistOfEmployee() {
		return listOfEmployee;
	}

	public void setlistOfEmployee(List<EmployeeInfo> listOfEmployee) {
		this.listOfEmployee = listOfEmployee;
	}

	
	public UserRoles getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(UserRoles userRoles) {
		this.userRoles = userRoles;
	}


	public List<Role> getFilteredRole() {
		return filteredRole;
	}


	public void setFilteredRole(List<Role> filteredRole) {
		this.filteredRole = filteredRole;
	}
	
	
}
