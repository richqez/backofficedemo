package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.EmployeeInfo;
import com.tgs.bc.model.Position;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.EmployeeSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class EmployeeCtl implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2074167178479672637L;

	// Screen Mapping Variable
	EmployeeInfo employeeInfo;

	// Variable for search
	EmployeeInfo sEmployeeInfo;

	// List Variable for DataTable
	List<EmployeeInfo> listOfEmployee;

	// List Variable for One Menu
	List<Position> listOfPosition;

	// inject Common-service For add update deleteW
	@Autowired
	CommonSv commonSv;

	@Autowired
	EmployeeSv employeeSv;

	@Autowired
	LoginCtl loginCtl;

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {

		listOfPosition = (List<Position>) commonSv.findAll(Position.class);
		employeeInfo = new EmployeeInfo();
		sEmployeeInfo = new EmployeeInfo();
		listOfEmployee = new ArrayList<EmployeeInfo>();
	}

	@SuppressWarnings("unchecked")
	public void refreshSelectOneMenu() {
		listOfPosition = (List<Position>) commonSv.findAll(Position.class);
	}

	public void save() {

		if (employeeInfo.getId() != 0) {
			employeeInfo.setUpdatedBy(loginCtl.getUser().getUserName());
			employeeInfo.setUpdatedDate(new Date());

		} else {

			employeeInfo.setCreatedDate(new Date());
			employeeInfo.setUpdatedDate(new Date());

			employeeInfo.setUpdatedBy(loginCtl.getUser().getUserName());
			employeeInfo.setUpdatedDate(new Date());
		}

		MsgStatus msgStatus = commonSv.saveAndUpdate(employeeInfo);

		if (msgStatus.getStatus()) {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		} else {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setFatalMessage().show();
		}

		init();

	}

	public void delete(EmployeeInfo employeeInfo) {

		MsgStatus msgStatus = commonSv.delete(employeeInfo);

		if (msgStatus.getStatus()) {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		} else {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setFatalMessage().show();
		}

		init();
	}

	public void update(EmployeeInfo employeeInfo) {

		setEmployeeInfo(employeeInfo);

	}

	public void search() {
		listOfEmployee = employeeSv.findByCriteria(sEmployeeInfo);
		sEmployeeInfo = new EmployeeInfo();
	}

	public EmployeeInfo getEmployeeInfo() {
		return employeeInfo;
	}

	public void setEmployeeInfo(EmployeeInfo employeeInfo) {
		this.employeeInfo = employeeInfo;
	}

	public List<EmployeeInfo> getListOfEmployee() {
		return listOfEmployee;
	}

	public void setListOfEmployee(List<EmployeeInfo> listOfEmployee) {
		this.listOfEmployee = listOfEmployee;
	}

	public List<Position> getListOfPosition() {
		return listOfPosition;
	}

	public void setListOfPosition(List<Position> listOfPosition) {
		this.listOfPosition = listOfPosition;
	}

	public EmployeeInfo getsEmployeeInfo() {
		return sEmployeeInfo;
	}

	public void setsEmployeeInfo(EmployeeInfo sEmployeeInfo) {
		this.sEmployeeInfo = sEmployeeInfo;
	}

}
