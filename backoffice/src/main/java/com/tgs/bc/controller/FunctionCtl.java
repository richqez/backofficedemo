package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Function;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.FunctionSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class FunctionCtl implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5132481339936718293L;

	Function function;

	Function sFunction;

	List<Function> listOfFunction;

	@Autowired
	CommonSv commonSv;

	@Autowired
	FunctionSv functionSv;

	@Autowired
	LoginCtl loginCtl;

	@PostConstruct
	public void init() {
		function = new Function();
		sFunction = new Function();
		listOfFunction = new ArrayList<Function>();
	}

	public void save() {

		if (function.getId() != 0) {

			function.setUpdatedBy(loginCtl.getUser().getUserName());
			function.setUpdatedDate(new Date());
		} else {

			function.setCreatedBy(loginCtl.getUser().getUserName());
			function.setCreatedDate(new Date());

			function.setUpdatedBy(loginCtl.getUser().getUserName());
			function.setUpdatedDate(new Date());
		}

		MsgStatus msgStatus = commonSv.saveAndUpdate(function);
		
		if (msgStatus.getStatus()) {

			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		} else {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
			.setFatalMessage().show();
		}
		init();

	}

	public void delete(Function function) {
		
		MsgStatus msgStatus = commonSv.delete(function);
		if (msgStatus.getStatus()) {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		}
		else{
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
			.setFatalMessage().show();
		}
		
		init();
	}

	public void update(Function function) {
		setFunction(function);
	}

	public void search() {
		listOfFunction = functionSv.findByCriteria(sFunction);
		sFunction = new Function();
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Function getsFunction() {
		return sFunction;
	}

	public void setsFunction(Function sFunction) {
		this.sFunction = sFunction;
	}

	public List<Function> getListOfFunction() {
		return listOfFunction;
	}

	public void setListOfFunction(List<Function> listOfFunction) {
		this.listOfFunction = listOfFunction;
	}

}
