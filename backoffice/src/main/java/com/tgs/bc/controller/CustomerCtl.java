package com.tgs.bc.controller;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.CustomerSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class CustomerCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4352139215007108227L;

	@Autowired
	CommonSv commonSv;

	@Autowired
	CustomerSv customerSv;

	@Autowired
	LoginCtl loginCtl;
	
	Customer customer;

	Customer sCustomer; // ใช้ในการค้นหา Customer

	List<Customer> listOfCustomer; // list Customer ที่ได้จากการค้นหา

	
	@PostConstruct
	public void init() {
		sCustomer = new Customer();
		customer = new Customer();
		listOfCustomer = new ArrayList<Customer>();
	}

	public void save() {

		if (customer.getId() != 0) {
			customer.setUpdatedBy(loginCtl.getUser().getUserName());
			customer.setUpdatedDate(new Date());
		}else {
			customer.setCreatedBy(loginCtl.getUser().getUserName());
			customer.setCreatedDate(new Date());
			
			customer.setUpdatedBy(loginCtl.getUser().getUserName());
			customer.setUpdatedDate(new Date());
		}
		
		MsgStatus msgStatus = commonSv.saveAndUpdate(customer);
		if (msgStatus.getStatus()) {

			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
		} else {
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
			.setFatalMessage().show();
		}
		
		init();
	}

	public void update(Customer customer) {
		this.customer = customer;
	}

	public void delete(Customer customer) {
		
		MsgStatus msgStatus = commonSv.delete(customer);
		if (msgStatus.getStatus()) {
			
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
					.setInfoMessage().show();
			
			
		}
		else{
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus)
			.setFatalMessage().show();
		}

		init();
	}

	public void search() {
	
		listOfCustomer = customerSv.findByCriteria(sCustomer);
		sCustomer = new Customer();
	}

	
	
	
	public List<Customer> getListOfCustomer() {
		return listOfCustomer;
	}

	public void setListOfCustomer(List<Customer> listOfCustomer) {
		this.listOfCustomer = listOfCustomer;
	}

	public CommonSv getCommonSv() {
		return commonSv;
	}

	public void setCommonSv(CommonSv commonSv) {
		this.commonSv = commonSv;
	}

	public Customer getCustomer() {
		return customer;
	}

	public Customer getsCustomer() {
		return sCustomer;
	}

	public void setsCustomer(Customer sCustomer) {
		this.sCustomer = sCustomer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}