package com.tgs.bc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.PurchaseOrder;
import com.tgs.bc.model.PurchaseOrderDetail;
import com.tgs.bc.model.Quotation;
import com.tgs.bc.model.QuotationDetail;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.PurchaseOrderSv;
import com.tgs.bc.service.QuotationSv;
import com.tgs.bc.utill.JsfMessagesUtils;

/**
 * @author Taiy
 *
 */
@Controller
@Scope("session")
public class PurchaseOrderCtl {

	PurchaseOrder sPurchaseOrder;

	public boolean selectCustomer;

	List<Quotation> listOfQuotation; // result quotation search

	List<Quotation> listOfQuotationFiltered;

	List<QuotationDetail> listOfQuotationDetail; // variable for show
	List<PurchaseOrder> listOfPurchaseOrders; // variable for show list in
												// datable
	List<QuotationDetail> listOfSelected; // listOfQuotation
	List<PurchaseOrderDetail> listOfPurchaseOrderDetail;

	Date sStartDate;
	Date sEndDate;

	CommonSearchDto<PurchaseOrder> commonSearchDto;

	PurchaseOrder purchaseOrder; // variable for map Purchase Order value

	@Autowired
	QuotationSv quotationSv; // QuotationService

	@Autowired
	PurchaseOrderSv purchaseOrderSv;

	@Autowired
	CommonSv commonSv;

	// initialize method automated call on start session
	@PostConstruct
	public void init() {

		purchaseOrder = new PurchaseOrder();
		listOfQuotation = new ArrayList<Quotation>();
		listOfPurchaseOrderDetail = new ArrayList<PurchaseOrderDetail>();
		commonSearchDto = new CommonSearchDto<PurchaseOrder>();
		sPurchaseOrder = new PurchaseOrder();
		selectCustomer = false;
		listOfPurchaseOrders = new ArrayList<PurchaseOrder>();
		sStartDate = null;
		sEndDate = null;

	}

	// method for search quotation

	// method for retrieve list of quotation from quotation
	public void getQuotationDetail(Quotation quotation) {
		listOfQuotationDetail = quotationSv
				.findQuotationDetailByQuotation(quotation);
	}

	// method for check quotation detail is is PurchaseOrderList / Ture : inList
	private boolean isInPurchaseOrderList(QuotationDetail quotation) {
		for (PurchaseOrderDetail purchaseOrderDetail : listOfPurchaseOrderDetail) {
			if (quotation.getId() == purchaseOrderDetail.getQuotationDetail()
					.getId()) {
				return true;
			}
		}
		return false;
	}

	public void selectCustomer(Customer customer) {
		this.purchaseOrder.setCustomer(customer);
		selectCustomer = true;
	}

	public void getQuotationByCustomer() {

		System.out.print("getQuotationByCustomer");

		if (purchaseOrder.getCustomer() != null) {
			listOfQuotation = quotationSv.findQuotationByCustomer(purchaseOrder
					.getCustomer());
			System.out.println("listOfQuotation" + listOfQuotation.size());

		} else {
			JsfMessagesUtils.getFacessMessage().setFatalMessage()
					.setHeadMessage("กรุณาเลือกลูกค้าก่อน")
					.show();															
		}
	}

	public void selectQuotationDetail() {

		for (QuotationDetail qtDetail : listOfSelected) {
			if (!isInPurchaseOrderList(qtDetail)) {
				PurchaseOrderDetail poDetail = new PurchaseOrderDetail();

				// key field
				poDetail.setQuotationDetail(qtDetail);
				poDetail.setPeriodDescription(qtDetail.getPeriodDescription());
				poDetail.setUnitType(qtDetail.getUnitType());
				poDetail.setDescription(qtDetail.getDescription());
				poDetail.setStartDate(qtDetail.getStartDate());
				poDetail.setEndDate(qtDetail.getEndDate());
				poDetail.setProjectName(qtDetail.getProjectName());
				poDetail.setUnitPrice(qtDetail.getUnitPrice());
				poDetail.setQuantity(qtDetail.getQuantity());
				poDetail.setAmount(qtDetail.getAmount());

				/*
				 * TO do vat per no set up
				 */

				listOfPurchaseOrderDetail.add(poDetail);

				poDetail = null;
			}
		}

		calculatePurchaseOrder();

		listOfSelected = null;

	}

	// method for search data PurchaseOrder From serverice
	public void searchPurchaseOrder() {

		commonSearchDto.setObject(sPurchaseOrder);

		Hashtable<String, Date> criteria = new Hashtable<String, Date>();

		if (getsStartDate() != null) {
			criteria.put(CommonSearchDto.START_DATE, getsStartDate());
		} else if (getsEndDate() != null) {
			criteria.put(CommonSearchDto.END_DATE, getsEndDate());
		}

		commonSearchDto.setCriteria(criteria);

		listOfPurchaseOrders = purchaseOrderSv.findByCriteria(commonSearchDto);
		sPurchaseOrder = new PurchaseOrder();

		sStartDate = null;
		sEndDate = null;

	}

	public void deletePoDetail(PurchaseOrderDetail detail) {
		listOfPurchaseOrderDetail.remove(detail);
	}

	public void calculatePurchaseOrder() {

		if (!listOfPurchaseOrderDetail.isEmpty()) {

			purchaseOrderSv.calculatePurchaseOrder(this.purchaseOrder,
					this.listOfPurchaseOrderDetail);

		}

	}

	public void savePurchaseOrder() {

		if (!listOfPurchaseOrderDetail.isEmpty()) {

			MsgStatus msgStatus = purchaseOrderSv.savePurchaseOrderAndDetail(
					purchaseOrder, listOfPurchaseOrderDetail);

			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();

			init();

		} else {
			JsfMessagesUtils.getFacessMessage()
					.setHeadMessage("PurchaseOrder_Detail should not empty")
					.setFatalMessage().show();
		}

	}

	public void updatePurchaseOrder(PurchaseOrder purchaseOrder) {

		setPurchaseOrder(null);
		listOfPurchaseOrderDetail = new ArrayList<PurchaseOrderDetail>();

		setPurchaseOrder(purchaseOrder);
		if (!purchaseOrder.getPurchaseOrderDetails().isEmpty()) {
			purchaseOrder.getPurchaseOrderDetails().forEach(
					pod -> listOfPurchaseOrderDetail.add(pod));
		}

		selectCustomer = true;

	}

	public void deletePurchaseOrder(PurchaseOrder purchaseOrder) {
		MsgStatus msgStatus = commonSv.delete(purchaseOrder);
		JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();
		init();
	}

	public List<Quotation> getListOfQuotation() {
		return listOfQuotation;
	}

	public void setListOfQuotation(List<Quotation> listOfQuotation) {
		this.listOfQuotation = listOfQuotation;
	}

	public List<QuotationDetail> getListOfQuotationDetail() {
		return listOfQuotationDetail;
	}

	public void setListOfQuotationDetail(
			List<QuotationDetail> listOfQuotationDetail) {
		this.listOfQuotationDetail = listOfQuotationDetail;
	}

	public List<PurchaseOrderDetail> getListOfPurchaseOrderDetail() {
		return listOfPurchaseOrderDetail;
	}

	public void setListOfPurchaseOrderDetail(
			List<PurchaseOrderDetail> listOfPurchaseOrderDetail) {
		this.listOfPurchaseOrderDetail = listOfPurchaseOrderDetail;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public List<QuotationDetail> getListOfSelected() {
		return listOfSelected;
	}

	public void setListOfSelected(List<QuotationDetail> listOfSelected) {
		this.listOfSelected = listOfSelected;
	}

	public List<PurchaseOrder> getListOfPurchaseOrders() {
		return listOfPurchaseOrders;
	}

	public void setListOfPurchaseOrders(List<PurchaseOrder> listOfPurchaseOrders) {
		this.listOfPurchaseOrders = listOfPurchaseOrders;
	}

	public PurchaseOrder getsPurchaseOrder() {
		return sPurchaseOrder;
	}

	public void setsPurchaseOrder(PurchaseOrder sPurchaseOrder) {
		this.sPurchaseOrder = sPurchaseOrder;
	}

	public Date getsStartDate() {
		return sStartDate;
	}

	public void setsStartDate(Date sStartDate) {
		this.sStartDate = sStartDate;
	}

	public Date getsEndDate() {
		return sEndDate;
	}

	public void setsEndDate(Date sEndDate) {
		this.sEndDate = sEndDate;
	}

	public boolean isSelectCustomer() {
		return selectCustomer;
	}

	public void setSelectCustomer(boolean selectCustomer) {
		this.selectCustomer = selectCustomer;
	}

	public List<Quotation> getListOfQuotationFiltered() {
		return listOfQuotationFiltered;
	}

	public void setListOfQuotationFiltered(
			List<Quotation> listOfQuotationFiltered) {
		this.listOfQuotationFiltered = listOfQuotationFiltered;
	}

}
