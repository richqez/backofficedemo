package com.tgs.bc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.TaxReceipt;
import com.tgs.bc.model.TaxReceiptDetail;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.InvoiceSv;
import com.tgs.bc.service.TaxReceiptSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class TaxReceiptCtl {

	TaxReceipt taxReceipt;

	TaxReceipt sTaxReceipt;

	TaxReceiptDetail aTaxReceiptDetail;

	List<String> refInvoiceNumberList;

	Customer customerSelected;

	boolean selectedCustomer = false;
	boolean selectedInvoice = false;

	List<Invoice> listOfInvoice;

	List<Invoice> listOfInvoiceFilltered;

	List<TaxReceiptDetail> listOfTaxReceiptDetails;

	List<Invoice> listOfSelectedInvoice;

	List<InvoiceDetail> listOfInvoiceDetail;

	List<InvoiceDetail> listOfSelectedInvoiceDetail;

	List<Invoice> listOfSelectedInvoiceForUpdate;

	List<TaxReceipt> listOfTaxReceipt;

	CommonSearchDto<TaxReceipt> commonSearch;

	@Autowired
	CommonSv commonSv;

	@Autowired
	TaxReceiptSv taxReceiptSv;

	@Autowired
	InvoiceSv invoiceSv;
	/*
	@Autowired
	LoginCtl loginCtl;
	*/
	@PostConstruct
	public void init() {
		listOfInvoiceDetail = new ArrayList<InvoiceDetail>();
		customerSelected = new Customer();
		taxReceipt = new TaxReceipt();
		aTaxReceiptDetail = new TaxReceiptDetail();
		listOfTaxReceiptDetails = new ArrayList<TaxReceiptDetail>();
		sTaxReceipt = new TaxReceipt();
		listOfInvoice = new ArrayList<Invoice>();
		listOfSelectedInvoice = new ArrayList<Invoice>();
		listOfSelectedInvoiceDetail = new ArrayList<InvoiceDetail>();
		listOfSelectedInvoiceForUpdate = new ArrayList<Invoice>();
		refInvoiceNumberList = new ArrayList<String>();
		selectedCustomer = false;
		selectedInvoice = false;
		commonSearch = new CommonSearchDto<TaxReceipt>();

	}

	public void selectCustomer(Customer customer) {

		init();

		setCustomerSelected(customer);
		JsfMessagesUtils.getFacessMessage()
				.setHeadMessage("เลือก Customer สำเร็จ").setInfoMessage()
				.show();
		selectedCustomer = true;

	}

	public void automatedCalculate() {
		taxReceiptSv.calculate(taxReceipt, listOfTaxReceiptDetails);
	}

	public void getInvoiceByCustomer() {

		if (!selectedCustomer) {
			JsfMessagesUtils.getFacessMessage()
					.setHeadMessage("กรุณาเลือก Customer ก่อนทำรายการนี้")
					.show();
		} else {

			listOfInvoice = invoiceSv
					.findInvoiceByCustomer(this.customerSelected);

		}

	}

	public void selectedInvoice() {
		listOfSelectedInvoice.forEach(invoice -> {

			invoice.getInvoiceDetails().forEach(idv -> {
				if (!listOfInvoiceDetail.contains(idv)) {
					listOfInvoiceDetail.add(idv);
				}
			});

			if (!refInvoiceNumberList.contains(invoice.getInvoiceNumber())) {
				refInvoiceNumberList.add(invoice.getInvoiceNumber());
			}

		});

		if (!listOfSelectedInvoice.isEmpty()) {
			selectedInvoice = true;
		} else {
			selectedInvoice = false;
		}

	}

	public void saveTaxReceipt() {

		if (taxReceipt.getId() != 0) {
			taxReceipt.setUpdatedDate(new Date());
			//taxReceipt.setUpdatedBy(loginCtl.getUser().getUserName());

		} else {
			taxReceipt.setCreatedDate(new Date());
			//taxReceipt.setCreatedBy(loginCtl.getUser().getUserName());
			taxReceipt.setUpdatedDate(new Date());
			//taxReceipt.setUpdatedBy(loginCtl.getUser().getUserName());

		}

		MsgStatus msgStatus = taxReceiptSv.saveTaxReceiptAndDetail(taxReceipt,
				listOfTaxReceiptDetails, listOfSelectedInvoice,
				listOfSelectedInvoiceForUpdate);

		JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();

		if (msgStatus.getStatus()) {
			init();
		}

	}

	public void updateTaxReceipt(TaxReceipt taxReceipt) {
		init();
		

		
		setTaxReceipt(taxReceipt);
		
		setListOfTaxReceiptDetails(getListOfTaxReceiptDetails());
		
		taxReceipt.getInvoices().forEach(iv -> {
			listOfInvoice.add(iv);
		});
		
		

	}

	public void selectedInvoiceDetail() {

		listOfSelectedInvoiceDetail.forEach(ivd -> {

			TaxReceiptDetail trxd = new TaxReceiptDetail();

			trxd.setUnitType(ivd.getUnitType());
			trxd.setQuantity(ivd.getQuantity());
			trxd.setUnitPrice(ivd.getUnitPrice());
			trxd.setAmount(ivd.getAmount());
			trxd.setDescription(ivd.getDescription());
			trxd.setPeriodDescription(ivd.getPeriodDescription());
			trxd.setProjectName(ivd.getProjectName());
			trxd.setStartDate(ivd.getStartDate());
			trxd.setEndDate(ivd.getStartDate());
			trxd.setRefInvoiceNumber(ivd.getInvoice().getInvoiceNumber());

			listOfTaxReceiptDetails.add(trxd);

		});

		taxReceiptSv.calculate(taxReceipt, listOfTaxReceiptDetails);

	}

	public void removeTaxReceiptDetail(TaxReceiptDetail taxReceiptDetail) {
		listOfTaxReceiptDetails.remove(taxReceiptDetail);
		taxReceiptSv.calculate(taxReceipt, listOfTaxReceiptDetails);
	}
	
	public void deleteTaxReceipt(TaxReceipt taxReceipt){
		taxReceiptSv.deleteTexReceipt(taxReceipt);
		init();
		listOfTaxReceipt = new ArrayList<TaxReceipt>();
	}

	public void getInvoiceDetailByListOfSelectedInvoice() {

		if (!selectedInvoice) {
			JsfMessagesUtils.getFacessMessage()
					.setHeadMessage("กรุณราเลือก Invoice ก่อนทำรายการ")
					.setFatalMessage().show();
		}

	}

	public void clearATaxReceiptDetail() {
		aTaxReceiptDetail = new TaxReceiptDetail();
	}

	public void manualAddTaxReceiptDetail() {

		aTaxReceiptDetail.setAmount(aTaxReceiptDetail.getQuantity()
				* aTaxReceiptDetail.getUnitPrice());

		listOfTaxReceiptDetails.add(aTaxReceiptDetail);

		taxReceiptSv.calculate(taxReceipt, listOfTaxReceiptDetails);

		clearATaxReceiptDetail();
	}

	public void removeInvoice(Invoice invoice) {

		listOfSelectedInvoice.remove(invoice);

		listOfInvoiceDetail = listOfInvoiceDetail.stream()
				.filter(ivd -> !ivd.getInvoice().equals(invoice))
				.collect(Collectors.toList());

		listOfSelectedInvoiceDetail = listOfSelectedInvoiceDetail.stream()
				.filter(ivs -> !ivs.getInvoice().equals(invoice))
				.collect(Collectors.toList());

		listOfTaxReceiptDetails = listOfTaxReceiptDetails
				.stream()
				.filter(trxd -> !trxd.getRefInvoiceNumber().equals(
						invoice.getInvoiceNumber()))
				.collect(Collectors.toList());

		refInvoiceNumberList = refInvoiceNumberList.stream()
				.filter(x -> !x.equals(invoice.getInvoiceNumber()))
				.collect(Collectors.toList());

		listOfSelectedInvoiceForUpdate.add(invoice);

		if (listOfSelectedInvoice.isEmpty()) {
			listOfInvoiceDetail = new ArrayList<InvoiceDetail>();
		}

		taxReceiptSv.calculate(taxReceipt, listOfTaxReceiptDetails);

	}

	public void searchTaxReceipt() {

		Hashtable<String, Date> critetia = new Hashtable<String, Date>();

		listOfTaxReceipt = taxReceiptSv.findByCritetia(commonSearch);

		sTaxReceipt = new TaxReceipt();
	}

	public TaxReceipt getTaxReceipt() {
		return taxReceipt;
	}

	public void setTaxReceipt(TaxReceipt taxReceipt) {
		this.taxReceipt = taxReceipt;
	}

	public List<Invoice> getListOfInvoice() {
		return listOfInvoice;
	}

	public void setListOfInvoice(List<Invoice> listOfInvoice) {
		this.listOfInvoice = listOfInvoice;
	}

	public List<TaxReceiptDetail> getListOfTaxReceiptDetails() {
		return listOfTaxReceiptDetails;
	}

	public void setListOfTaxReceiptDetails(
			List<TaxReceiptDetail> listOfTaxReceiptDetails) {
		this.listOfTaxReceiptDetails = listOfTaxReceiptDetails;
	}

	public List<Invoice> getListOfInvoiceFilltered() {
		return listOfInvoiceFilltered;
	}

	public void setListOfInvoiceFilltered(List<Invoice> listOfInvoiceFilltered) {
		this.listOfInvoiceFilltered = listOfInvoiceFilltered;
	}

	public List<Invoice> getListOfSelectedInvoice() {
		return listOfSelectedInvoice;
	}

	public void setListOfSelectedInvoice(List<Invoice> listOfSelectedInvoice) {
		this.listOfSelectedInvoice = listOfSelectedInvoice;
	}

	public Customer getCustomerSelected() {
		return customerSelected;
	}

	public void setCustomerSelected(Customer customerSelected) {
		this.customerSelected = customerSelected;
	}

	public boolean isSelectedCustomer() {
		return selectedCustomer;
	}

	public void setSelectedCustomer(boolean selectedCustomer) {
		this.selectedCustomer = selectedCustomer;
	}

	public List<InvoiceDetail> getListOfInvoiceDetail() {
		return listOfInvoiceDetail;
	}

	public void setListOfInvoiceDetail(List<InvoiceDetail> listOfInvoiceDetail) {
		this.listOfInvoiceDetail = listOfInvoiceDetail;
	}

	public boolean isSelectedInvoice() {
		return selectedInvoice;
	}

	public void setSelectedInvoice(boolean selectedInvoice) {
		this.selectedInvoice = selectedInvoice;
	}

	public List<InvoiceDetail> getListOfSelectedInvoiceDetail() {
		return listOfSelectedInvoiceDetail;
	}

	public void setListOfSelectedInvoiceDetail(
			List<InvoiceDetail> listOfSelectedInvoiceDetail) {
		this.listOfSelectedInvoiceDetail = listOfSelectedInvoiceDetail;
	}

	public TaxReceiptDetail getaTaxReceiptDetail() {
		return aTaxReceiptDetail;
	}

	public void setaTaxReceiptDetail(TaxReceiptDetail aTaxReceiptDetail) {
		this.aTaxReceiptDetail = aTaxReceiptDetail;
	}

	public List<String> getRefInvoiceNumberList() {
		return refInvoiceNumberList;
	}

	public void setRefInvoiceNumberList(List<String> refInvoiceNumberList) {
		this.refInvoiceNumberList = refInvoiceNumberList;
	}

	public List<TaxReceipt> getListOfTaxReceipt() {
		return listOfTaxReceipt;
	}

	public void setListOfTaxReceipt(List<TaxReceipt> listOfTaxReceipt) {
		this.listOfTaxReceipt = listOfTaxReceipt;
	}

}
