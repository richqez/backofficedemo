package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Bill;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.service.BillSv;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class BillCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1899859649049343444L;

	@Autowired
	BillSv billSv;

	@Autowired
	CommonSv commonSv;

	@Autowired
	LoginCtl loginCtl;

	CommonSearchDto<Bill> commonSearchDto;

	Bill bill;

	Bill sBill; // ใช้ในการค้นหา Bill

	List<Bill> listOfBill; // list Bill ที่ได้จากการค้นหา

	List<Invoice> listOfInvoice; // list invoice ทั้งหมด

	List<Invoice> filteredInvoice; // ตัวกรอง Invoice ใช้ร่วมกับ Data table 

	List<Invoice> listSelectInvoice; // list ที่ได้จาก เลือก Invoice
	
	List<Invoice> listInvoiceForUpdate; // list invoice ในเคส Update

	Date sStartDate; // วันที่ เริ่มต้น ในเคส ค้นหา
	Date sEndDate; // วันที่ สิ้นสุด ในเคส ค้นหา

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {

		bill = new Bill();
		sBill = new Bill();
		listOfBill = new ArrayList<Bill>();
		listOfInvoice =  (List<Invoice>) commonSv.findAllOrderBy(Invoice.class ,"id");
		listSelectInvoice = new ArrayList<Invoice>();
		commonSearchDto = new CommonSearchDto<Bill>();
		listInvoiceForUpdate = new ArrayList<Invoice>();

	}

	public void search() {
		commonSearchDto.setObject(sBill);
		Hashtable<String, Date> criteria = new Hashtable<String, Date>();

		if (getsStartDate() != null) {
			criteria.put(CommonSearchDto.START_DATE, getsStartDate());

		} else if (getsEndDate() != null) {
			criteria.put(CommonSearchDto.END_DATE, getsEndDate());
		}

		commonSearchDto.setCriteria(criteria);

		listOfBill = billSv.findByCriteria(commonSearchDto);
		sBill = new Bill();
		
	}

	public void saveBill() {

		if (bill.getId() != 0) {
			bill.setUpdatedBy(loginCtl.getUser().getUserName());
			bill.setUpdatedDate(new Date());
		} else {
			bill.setCreatedBy(loginCtl.getUser().getUserName());
			bill.setCreatedDate(new Date());
			bill.setBillDate(new Date());

			bill.setUpdatedBy(loginCtl.getUser().getUserName());
			bill.setUpdatedDate(new Date());
		}
		
		if (!listSelectInvoice.isEmpty()) {
			
			MsgStatus msgStatus = billSv.saveBillAndInvoice(bill, listSelectInvoice,listInvoiceForUpdate);
			
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();

			init();
			
		}else {
			
			JsfMessagesUtils.getFacessMessage()
			.setHeadMessage("กรุณาเลือกใบแจ้งหนี้")
			.setFatalMessage().show();
		}
		
		
		
	}

	public void update(Bill bill) {
		listSelectInvoice = new ArrayList<Invoice>();
		
		this.bill=bill;
		
		if (bill.getId() != 0) {
			List<Invoice> invoices = billSv.findInvoiceByBill(bill);
			for (Invoice iv : invoices) {
				listSelectInvoice.add(iv);
			}
		}
	}

	public void delete(Bill bill) {
		
		for (Invoice invoice : listOfInvoice) {
			if (invoice.getBill() == bill.getInvoices()) {
				invoice.setBill(null);
				commonSv.saveAndUpdate(invoice);
			}
		}
		
		commonSv.delete(bill);
		init();
	}

	public void removeInvoice(Invoice invoice) {
		listSelectInvoice.remove(invoice);
		listInvoiceForUpdate.add(invoice);
	}

	public Boolean isList(Invoice invoice) {
		for (Invoice iv : listSelectInvoice) {
			if (invoice.getId() == iv.getId()) {
				return true;
			}
		}
		return false;
	}

	public void seleteInvoice(Invoice invoice) {
		if (!isList(invoice)) {
			if (invoice.getBill() == null ) {
				listSelectInvoice.add(invoice);
			}else {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, "ใบแจ้งหนี้ ถูกใช้งานแล้ว",
								""));
			}
			
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_FATAL, "ไม่สามารถเลือกใบแจ้งหนี้ได้ เนื่องจากอาจจะถูกใช้งานแล้ว!",
							""));
		}

	}

	// Getter Setter

	public Bill getsBill() {
		return sBill;
	}

	public void setsBill(Bill sBill) {
		this.sBill = sBill;
	}

	public List<Bill> getListOfBill() {
		return listOfBill;
	}

	public void setListOfBill(List<Bill> listOfBill) {
		this.listOfBill = listOfBill;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public List<Invoice> getListOfInvoice() {
		return listOfInvoice;
	}

	public void setListOfInvoice(List<Invoice> listOfInvoice) {
		this.listOfInvoice = listOfInvoice;
	}

	public Date getsStartDate() {
		return sStartDate;
	}

	public void setsStartDate(Date sStartDate) {
		this.sStartDate = sStartDate;
	}

	public Date getsEndDate() {
		return sEndDate;
	}

	public void setsEndDate(Date sEndDate) {
		this.sEndDate = sEndDate;
	}

	public List<Invoice> getFilteredInvoice() {
		return filteredInvoice;
	}

	public void setFilteredInvoice(List<Invoice> filteredInvoice) {
		this.filteredInvoice = filteredInvoice;
	}

	public List<Invoice> getListSelectInvoice() {
		return listSelectInvoice;
	}

	public void setListSelectInvoice(List<Invoice> listSelectInvoice) {
		this.listSelectInvoice = listSelectInvoice;
	}

}
