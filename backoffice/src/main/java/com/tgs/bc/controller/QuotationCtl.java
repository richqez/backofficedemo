package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Quotation;
import com.tgs.bc.model.QuotationDetail;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.QuotationSv;
import com.tgs.bc.utill.JsfMessagesUtils;

@Controller
@Scope("session")
public class QuotationCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3930443338302558926L;

	@Autowired
	QuotationSv quotationSv;

	@Autowired
	CommonSv commonSv;
	
	@Autowired
	LoginCtl loginCtl;
	
	Quotation quotation;
	
	CommonSearchDto<Quotation> commonSearchDto; // การค้นหาแบบใช้เวลา
	
	Quotation sQuotation; // ใช้ในการค้นหา Quotation

	QuotationDetail quotationDetail; 

	List<Quotation> listOfQuotation; // list Quotation ที่ได้จากการค้นหา

	List<Customer> listOfCustomer; // list Customer ทั้งหมด แสดงในรูปแบบ OneMenu

	List<Customer> filteredCustomer; // ตัวกรอง Customer ใช้ร่วมกับ Data table 
	
	List<QuotationDetail> listOfQuotationDetail; // list QuotationDetail ที่ได้จากการเพิ่ม

	Date sStartDate;
	Date sEndDate;
	
	
	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {

		
		listOfCustomer = (List<Customer>) commonSv.findAll(Customer.class);
		quotation = new Quotation();
		sQuotation = new Quotation();
		commonSearchDto = new CommonSearchDto<Quotation>();
		quotation.setVatStatus(true);
		listOfQuotation = new ArrayList<Quotation>();
		listOfQuotationDetail = new ArrayList<QuotationDetail>();
	}

	public void save() {
		
		if (quotation.getCustomer() == null || listOfQuotationDetail == null 
				|| quotation.getTotal() == null || quotation.getGrandTotal()== null) {
			FacesContext.getCurrentInstance().addMessage
						(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
								"กรุณากรอกข้อมูลให้ครบ ","" ));
		}else {
			
			if (quotation.getId() != 0) {
				quotation.setUpdatedBy(loginCtl.getUser().getUserName());
				quotation.setUpdatedDate(new Date());
			}else {
				quotation.setCreatedBy(loginCtl.getUser().getUserName());
				quotation.setCreatedDate(new Date());
				
				quotation.setUpdatedBy(loginCtl.getUser().getUserName());
				quotation.setUpdatedDate(new Date());
			}
			
		if (!listOfQuotationDetail.isEmpty()) {
			
			MsgStatus msgStatus = quotationSv.saveQuotationDetail(quotation, listOfQuotationDetail);
			
			JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();
			
			init();
		
		}else {
			JsfMessagesUtils.getFacessMessage()
			.setHeadMessage("กรุณาใส่รายละเอียดใบเสนอราคา")
			.setFatalMessage().show();
		}	
			
		
		}
			
	}
	
	public void update(Quotation quotation){
		listOfQuotationDetail = new ArrayList<QuotationDetail>();
		
		setQuotation(quotation);
		
		if(quotation.getId()!=0){
			List<QuotationDetail> quotationDetails = quotationSv.findQuotationDetailByQuotation(quotation);
			for (QuotationDetail quotationDetail : quotationDetails) {
				listOfQuotationDetail.add(quotationDetail);
			}
		}
	}
	
	
	//ลบไม่ได้ เพราะติดความสัมพันธ์  กับ ดีเทล
	public void delete(Quotation quotation) {
		commonSv.delete(quotation);
		init();
	}
	
	public void search() {
		commonSearchDto.setObject(sQuotation);
		
		Hashtable<String, Date> criteria = new Hashtable<String, Date>();
	
		if (getsStartDate() != null) {
			criteria.put(CommonSearchDto.START_DATE, getsStartDate());
			
		}else if (getsEndDate() != null) {
			criteria.put(CommonSearchDto.END_DATE, getsEndDate());
		}
		
		commonSearchDto.setCriteria(criteria);
		
		listOfQuotation = quotationSv.findByCriteria(commonSearchDto);
		sQuotation = new Quotation();

	}
	
	private boolean isInQtDetailList(QuotationDetail quotationDetail){
		
		for (QuotationDetail detail : listOfQuotationDetail) {
			if(detail.equals(quotationDetail)){
				return true;
			}
		}
		return false;
	}
	
	public List<QuotationDetail> seleteQuotationDetail(){
			
			quotationDetail.setAmount(quotationDetail.getUnitPrice()*quotationDetail.getQuantity());
			if (!isInQtDetailList(quotationDetail)) {
				listOfQuotationDetail.add(quotationDetail);
			}
			RequestContext.getCurrentInstance().closeDialog(null);
			
			
			return listOfQuotationDetail;			
	}	
	
	//Remove QuotationDetail from listOfQuotationDetail 
	
	public void removeDetail(QuotationDetail quotationDetail) {
		listOfQuotationDetail.remove(quotationDetail);
	}
	
	public void seleteCustomer(Customer customer) {
		
		getQuotation().setCustomer(customer);
			
	}
	
	//calculate 
	
	public void sumProduct(){
			if (listOfQuotationDetail.size() == 0) {
			
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage
						.SEVERITY_WARN, "กรุณาเพิ่่มรายละเอียดใบเสนอราคา" , ""));	
			
			}else {
				quotationSv.calTotal(listOfQuotationDetail, quotation);
			} 
			
	}
	
	public void calGrandtotal(){
		
		if (quotation.getTotal() != null  ) {
			quotationSv.calGrandtotal(quotation);	
			
			if (quotation.getGrandTotal() < 0) {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_WARN, 
						"กรุณาตรวจสอบส่วนลด ", ""));
				
				quotation.setGrandTotal(null);
		}
				
		}else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(
					FacesMessage.SEVERITY_WARN, "กรูณากดปุ่มคำนวณราคา" ,""));
		}
	
	}
	
	
	@SuppressWarnings("unused")
	private void onChangeVat(){
		if (quotation.getVatStatus() == true) {
			quotation.setVatPercentage(0.0);
		}
	}
	
	//case add quotationDetail 
	
	public void dialogaddItem() {
	
		quotationDetail = new QuotationDetail();
		
		Map<String, Object> options = new HashMap<String, Object>();
		options.put("modal", true);
		options.put("draggable", false);
		options.put("resizable", false);
		options.put("contentHeight", 400);
		options.put("contentWidth", 700);
		RequestContext.getCurrentInstance().openDialog("addproduct", options,null);

	}
	
	// case edit quotationDetail
	public void dialogaddItem(QuotationDetail quotationDetail) {
		setQuotationDetail(quotationDetail);
		Map<String, Object> options = new HashMap<String, Object>();
		options.put("modal", true);
		options.put("draggable", false);
		options.put("resizable", false);
		options.put("contentHeight", 400);
		options.put("contentWidth", 700);
		RequestContext.getCurrentInstance().openDialog("addproduct", options,null);

	}
	
	
	//GETTER SETTER
	
	public QuotationDetail getquotationDetail() {
		return quotationDetail;
	}

	public void setquotationDetail(QuotationDetail detail) {
		this.quotationDetail = detail;
	}

	public List<QuotationDetail> getListOfQuotationDetail() {
		return listOfQuotationDetail;
	}

	public void setListOfQuotationDetail(List<QuotationDetail> listOfQuotationDetail) {
		this.listOfQuotationDetail = listOfQuotationDetail;
	}

	public Quotation getsQuotation() {
		return sQuotation;
	}

	public void setsQuotation(Quotation sQuotation) {
		this.sQuotation = sQuotation;
	}

	public Quotation getQuotation() {
		
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	public QuotationDetail getQuotationDetail() {
		return quotationDetail;
	}

	public void setQuotationDetail(QuotationDetail quotationDetail) {
		this.quotationDetail = quotationDetail;
	}

	public List<Quotation> getListOfQuotation() {
		return listOfQuotation;
	}

	public void setListOfQuotation(List<Quotation> listOfQuotation) {
		this.listOfQuotation = listOfQuotation;
	}

	public List<Customer> getListOfCustomer() {
		return listOfCustomer;
	}

	public void setListOfCustomer(List<Customer> listOfCustomer) {
		this.listOfCustomer = listOfCustomer;
	}

	public Date getsStartDate() {
		return sStartDate;
	}

	public void setsStartDate(Date sStartDate) {
		this.sStartDate = sStartDate;
	}

	public Date getsEndDate() {
		return sEndDate;
	}

	public void setsEndDate(Date sEndDate) {
		this.sEndDate = sEndDate;
	}

	public List<Customer> getFilteredCustomer() {
		return filteredCustomer;
	}

	public void setFilteredCustomer(List<Customer> filteredCustomer) {
		this.filteredCustomer = filteredCustomer;
	}

	
	
}