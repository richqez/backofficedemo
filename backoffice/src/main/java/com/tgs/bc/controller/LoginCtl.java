package com.tgs.bc.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.model.User;
import com.tgs.bc.service.UserSv;

@ManagedBean
@Controller
@SessionScoped
public class LoginCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4359343811017525458L;

	Boolean isLogin = false;
	User user;

	@Autowired
	UserSv userSv;

	@PostConstruct
	public void init() {
		user = new User();
	}

	public void login() throws IOException {

		User result = userSv.login(user);

		if (result != null) {
			user = result;
			isLogin = true;
			
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
			context.redirect(context.getRequestContextPath() + "/views/po/po.xhtml");
			
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"Error User Or Password Invalid", ""));
		}

	}

	public void logout() {
		this.user = null ;
		isLogin = false ;
	}

	public Boolean getIsLogin() {
		return isLogin;
	}

	public void setIsLogin(Boolean isLogin) {
		this.isLogin = isLogin;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
