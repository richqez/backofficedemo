package com.tgs.bc.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.tgs.bc.dto.FunctionWorkRightDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Function;
import com.tgs.bc.model.Permission;
import com.tgs.bc.model.Role;
import com.tgs.bc.model.Workright;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.FunctionSv;
import com.tgs.bc.service.RoleSv;
import com.tgs.bc.utill.JsfMessagesUtils;
import com.tgs.bc.utill.WorkRightFactory;
import com.tgs.bc.utill.WorkRightFactoryImpl;

/**
 * @author Taiy
 *
 */
@Controller
@Scope("session")
public class RoleCtl implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1027424085672755011L;
	Role role;
	Role sRole;
	List<Workright> listOfWorkRight;
	List<Workright> listOfSelectWorkRight;
	List<Function> listOfFilltered;
	List<Function> listOfFunction;
	List<Role> listOfRole;

	// list DTO Variable
	List<FunctionWorkRightDto> listOfFunctionAndWorkRight;

	// DTO Variable
	FunctionWorkRightDto functionAndWorkRight;

	WorkRightFactory workRightFactory = new WorkRightFactoryImpl();

	List<FunctionWorkRightDto> dtoForShowRoleDetail;

	@Autowired
	CommonSv commonSv;
	@Autowired
	FunctionSv functionSv;
	@Autowired
	RoleSv roleSv;
	@Autowired
	LoginCtl loginCtl;

	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		role = new Role();
		sRole = new Role();
		listOfRole = new ArrayList<Role>();
		listOfFunctionAndWorkRight = new ArrayList<FunctionWorkRightDto>();
		functionAndWorkRight = new FunctionWorkRightDto();
		listOfWorkRight = (List<Workright>) commonSv.findAll(Workright.class);
		listOfFunction = (List<Function>) commonSv.findAll(Function.class);
		dtoForShowRoleDetail = new ArrayList<FunctionWorkRightDto>();
	}

	public boolean isValidationWorkright() {

		for (FunctionWorkRightDto dto : listOfFunctionAndWorkRight) {
			if (dto.getWorkrights() == null || dto.getWorkrights().size() < 1) {
				JsfMessagesUtils
						.getFacessMessage()
						.setHeadMessage(
								"คุณยังไม่ได้กำหนดสิทธิในการใช้งาน ที่ Function "
										+ dto.getFunction().getName())
						.setFatalMessage().show();
				return false;
			}
		}
		return true;

	}

	public void save() {

		if (isValidationWorkright()) {
			if (role.getId() != 0) {
				role.setUpdatedBy(loginCtl.getUser().getUserName());
				role.setUpdatedDate(new Date());

			} else {
				role.setCreatedBy(loginCtl.getUser().getUserName());
				role.setCreatedDate(new Date());

				role.setUpdatedBy(loginCtl.getUser().getUserName());
				role.setUpdatedDate(new Date());
			}

			if (!listOfFunction.isEmpty()) {
				
				MsgStatus msgStatus = roleSv.saveRolePermissions(this.role,
						this.listOfFunctionAndWorkRight);
				
				JsfMessagesUtils.getFacessMessage().setMsgStatus(msgStatus).show();

				init();
				
			}else {
				JsfMessagesUtils.getFacessMessage()
				.setHeadMessage("Function should not empty")
				.setFatalMessage().show();
			}
			
		

		}
	}

	public void delete(Role role) {
		commonSv.delete(role);
	}

	public void setWorkRight(FunctionWorkRightDto dto) {

		setFunctionAndWorkRight(dto);

		setListOfSelectWorkRight(new ArrayList<Workright>());

		if (functionAndWorkRight.getWorkrights() != null) {

			setListOfSelectWorkRight(functionAndWorkRight.getWorkrights());

		}

	}

	public void selectFunction(Function function) {

		if (!isInListFunctionAndWorkRight(function)) {

			this.functionAndWorkRight = new FunctionWorkRightDto();
			this.functionAndWorkRight.setFunction(function);
			this.listOfFunctionAndWorkRight.add(functionAndWorkRight);
			this.functionAndWorkRight = null;

		} else {
			JsfMessagesUtils
					.getFacessMessage()
					.setHeadMessage(
							"คุณไม่สามารถเลือก Function" + function.getName()
									+ " ที่มีอยู่แล้ว กรุณาลองใหม่")
					.setErrorMessage().show();
		}

	}

	public void removeDtoItemList(FunctionWorkRightDto dto) {

		listOfFunctionAndWorkRight.remove(dto);
	}

	public void showRoleDetail(Role role) {

		dtoForShowRoleDetail = new ArrayList<FunctionWorkRightDto>();

		List<Permission> listOfPermisstionForShowDetail = roleSv
				.findPermissionByRole(role);

		List<Workright> listWorkrights = null;

		FunctionWorkRightDto fwDto = null;

		Function function = new Function();

		for (Permission permission : listOfPermisstionForShowDetail) {

			if (function.getId() != permission.getFunction().getId()) {
				listWorkrights = new ArrayList<Workright>();
				fwDto = new FunctionWorkRightDto();
				fwDto.setFunction(permission.getFunction());
				fwDto.setWorkrights(listWorkrights);
				listWorkrights.add(permission.getWorkright());
				function = permission.getFunction();
				dtoForShowRoleDetail.add(fwDto);

			} else {
				listWorkrights.add(permission.getWorkright());
			}

		}

	}

	public void update(Role role) {

		List<Permission> listOfPermission = roleSv.findPermissionByRole(role);

		listOfFunctionAndWorkRight = new ArrayList<FunctionWorkRightDto>();
		FunctionWorkRightDto fWRDto = null;
		Function fc = new Function();
		List<Workright> workrights = null;

		Set<Permission> setOfPermissions = new HashSet<Permission>(
				listOfPermission);

		this.setRole(role);
		this.getRole().setPermissions(setOfPermissions);

		for (Permission permission : listOfPermission) {
			if (fc.getId() != permission.getFunction().getId()) {
				fWRDto = new FunctionWorkRightDto();
				workrights = new ArrayList<Workright>();
				fWRDto.setFunction(permission.getFunction());
				fWRDto.setWorkrights(workrights);
				fWRDto.getWorkrights().add(permission.getWorkright());
				listOfFunctionAndWorkRight.add(fWRDto);
				fc = permission.getFunction();
			} else {
				fWRDto.getWorkrights().add(permission.getWorkright());
			}
		}

	}

	public void search() {

		listOfRole = roleSv.findByCriteria(sRole);
		sRole = new Role();

	}

	public void selectWorkRight() {

		functionAndWorkRight.setWorkrights(listOfSelectWorkRight);

		functionAndWorkRight = null;

	}

	private boolean isInListFunctionAndWorkRight(Function function) {

		for (FunctionWorkRightDto fw : listOfFunctionAndWorkRight) {
			if (function.getId() == fw.getFunction().getId()) {
				return true;
			}
		}

		return false;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Role getsRole() {
		return sRole;
	}

	public void setsRole(Role sRole) {
		this.sRole = sRole;
	}

	public List<Workright> getListOfWorkRight() {
		return listOfWorkRight;
	}

	public void setListOfWorkRight(List<Workright> listOfWorkRight) {
		this.listOfWorkRight = listOfWorkRight;
	}

	public List<Function> getListOfFunction() {
		return listOfFunction;
	}

	public void setListOfFunction(List<Function> listOfFunction) {
		this.listOfFunction = listOfFunction;
	}

	public List<FunctionWorkRightDto> getListOfFunctionAndWorkRight() {
		return listOfFunctionAndWorkRight;
	}

	public void setListOfFunctionAndWorkRight(
			List<FunctionWorkRightDto> listOfFunctionAndWorkRight) {
		this.listOfFunctionAndWorkRight = listOfFunctionAndWorkRight;
	}

	public List<Workright> getListOfSelectWorkRight() {
		return listOfSelectWorkRight;
	}

	public void setListOfSelectWorkRight(List<Workright> listOfSelectWorkRight) {
		this.listOfSelectWorkRight = listOfSelectWorkRight;
	}

	public List<Role> getListOfRole() {
		return listOfRole;
	}

	public void setListOfRole(List<Role> listOfRole) {
		this.listOfRole = listOfRole;
	}

	public FunctionWorkRightDto getFunctionAndWorkRight() {
		return functionAndWorkRight;
	}

	public void setFunctionAndWorkRight(
			FunctionWorkRightDto functionAndWorkRight) {
		this.functionAndWorkRight = functionAndWorkRight;
	}

	public List<Function> getListOfFilltered() {
		return listOfFilltered;
	}

	public void setListOfFilltered(List<Function> listOfFilltered) {
		this.listOfFilltered = listOfFilltered;
	}

	public List<FunctionWorkRightDto> getDtoForShowRoleDetail() {
		return dtoForShowRoleDetail;
	}

	public void setDtoForShowRoleDetail(
			List<FunctionWorkRightDto> dtoForShowRoleDetail) {
		this.dtoForShowRoleDetail = dtoForShowRoleDetail;
	}

}
