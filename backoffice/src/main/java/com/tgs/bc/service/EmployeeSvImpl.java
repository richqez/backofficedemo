package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.EmployeeDao;
import com.tgs.bc.model.EmployeeInfo;

@Service
@Transactional
public class EmployeeSvImpl implements EmployeeSv {


	/**
	 * 
	 */
	private static final long serialVersionUID = 909830693037338721L;
	@Autowired
	EmployeeDao employeeDao ;

	public List<EmployeeInfo> findByCriteria(EmployeeInfo employeeInfo) {
		return employeeDao.findByCriteria(employeeInfo);
	}
	


}
