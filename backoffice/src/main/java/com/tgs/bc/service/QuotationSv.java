package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Quotation;
import com.tgs.bc.model.QuotationDetail;

public interface QuotationSv extends Serializable {

	public List<Quotation> findByCriteria(Quotation quotation);
	public List<QuotationDetail> findQuotationDetailByQuotation(Quotation quotation);
	public MsgStatus saveQuotationDetail(Quotation quotation,List<QuotationDetail> listOfQuotationDetail);
	public List<Quotation> findByCriteria(CommonSearchDto<Quotation> commonSearchDto);
	public Quotation calGrandtotal(Quotation quotation);
	public Quotation calTotal(List<QuotationDetail> list,Quotation quotation);
	public List<Quotation> findQuotationByCustomer(Customer customer);
}