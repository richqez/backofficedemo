package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.MsgStatus;



public interface CommonSv extends Serializable  {

	public MsgStatus saveAndUpdate(Object object);
	
	public MsgStatus delete(Object object);

	public List<?> findAll(Class<?> clazz);
	
	public Object findById(Class<?> clazz,int id);
	
	public Object findById(Class<?> clazz,String id);
	
	public Object findByIdCode(Class<?> clazz,String id);

	public List<?> findAllOrderBy(Class<?> clazz , String id);
	
		
	
}
