package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.PurchaseOrder;
import com.tgs.bc.model.PurchaseOrderDetail;


public interface PurchaseOrderSv extends Serializable {

	public List<PurchaseOrder> findByCriteria(
			CommonSearchDto<PurchaseOrder> commonSearchDto);
	
	public MsgStatus savePurchaseOrderAndDetail(PurchaseOrder po,
			List<PurchaseOrderDetail> details);
	
	public List<PurchaseOrder> findAllPurchaseOrderAndDetail();
	
	public void calculatePurchaseOrder(PurchaseOrder purchaseOrder,List<PurchaseOrderDetail> purchaseOrderDetails);

}
