package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.PositionDao;
import com.tgs.bc.model.Position;

@Service
@Transactional
public class PositionSvImpl implements PositionSv{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1818567027837659479L;
	@Autowired
	PositionDao positionDao ;

	public List<Position> findByCriteria(Position position) {
		return positionDao.findByCriteria(position);
	}
	


}
