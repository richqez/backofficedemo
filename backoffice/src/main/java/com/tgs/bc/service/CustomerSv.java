package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.model.Customer;


public interface CustomerSv extends Serializable {

	
	public List<Customer> findByCriteria (Customer customer);
	public List<Customer> findByCriteriaById (Customer customer);
}
