/**
 * 
 */
package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.PurchaseOrder;

/**
 * @author Taiy
 *
 */
public interface InvoiceSv extends Serializable {

	public List<Invoice> findByCriteria(CommonSearchDto<Invoice> searchDto);

	public MsgStatus saveInvoiceAndInvoiceDetail(Invoice invoice,
			List<InvoiceDetail> detail,
			PurchaseOrder purchaseOrder);

	public Invoice calculateInvoice(Invoice invoice,
			List<InvoiceDetail> invoiceDetails);
	
	public MsgStatus approved(Invoice invoice);
	
	public MsgStatus unApproved(Invoice invoice);
	
	public List<Invoice> findInvoiceByCustomer(Customer customer);
	
}