package com.tgs.bc.service;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.RoleDao;
import com.tgs.bc.dto.FunctionWorkRightDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Permission;
import com.tgs.bc.model.Role;
import com.tgs.bc.model.Workright;

@Service
@Transactional
public class RoleSvImpl implements RoleSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 875706284318403112L;

	MsgStatus msgStatus;

	@Autowired
	RoleDao roleDao;

	@Autowired
	CommonSv commonSv;

	@Autowired
	FunctionSv functionSv;

	public List<Role> findByCriteria(Role role) {
		return roleDao.findByCriteria(role);
	}

	@Override
	public MsgStatus saveRolePermissions(Role role,
			List<FunctionWorkRightDto> dtos) {

		msgStatus = new MsgStatus();

		try {

			if (!role.getPermissions().isEmpty()) {
				role.getPermissions().forEach(r -> commonSv.delete(r));

			}

			commonSv.saveAndUpdate(role);

			for (FunctionWorkRightDto functionWorkRightDto : dtos) {
				for (Workright workright : functionWorkRightDto.getWorkrights()) {
					Permission permission = new Permission();
					permission.setFunction(functionWorkRightDto.getFunction());
					permission.setRole(role);
					permission.setWorkright(workright);
					commonSv.saveAndUpdate(permission);
				}

			}
			msgStatus.setStatusStr("ทำการบันทึกข้อมูล "+role.getName()+" สำเร็จ");
			msgStatus.setStatus(true);

		} catch (Exception e) {
			e.printStackTrace();
			msgStatus.setMessage(e.toString());
			msgStatus.setStatus(false);

		}

		return msgStatus;

	}

	public List<Permission> findPermissionByRole(Role role) {
		return roleDao.findPermissionByRole(role);
	}

}
