package com.tgs.bc.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.MenuDao;
import com.tgs.bc.model.Menu;

@Service
@Transactional
public class MenuSvImpl implements MenuSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6636817773648004328L;
	@Autowired
	MenuDao menuDao;

	public List<Menu> getAllMenuNotLv3() {

		List<Menu> list = menuDao.getAllMenuNotLv3();

		Menu dummy = new Menu();

		dummy.setId(-99);
		dummy.setName("NoParent");
		dummy.setLevel(0);
		dummy.setMenu(null);

		list.add(dummy);

		Collections.sort(list, new Comparator<Menu>() {

			public int compare(Menu o1, Menu o2) {
				return ((Integer) o1.getId()).compareTo((Integer) o2.getId());

			}
		});

		return list;
	}

	
	public List<Menu> getAll() {
		return  menuDao.getAll();
	}

}
