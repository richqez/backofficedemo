package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.FunctionDao;
import com.tgs.bc.model.Function;

@Service
@Transactional
public class FunctionSvImpl implements FunctionSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3166829930043111070L;
	@Autowired
	FunctionDao functionDao;
	
	public List<Function> findByCriteria(Function function) {
		return functionDao.findByCriteria(function);
	}

	public List<Function> getAllNotInMenu() {
		
		List<Function> functions = functionDao.getAllNotInMenu();
		
		if (functions.size()==0) {
			Function function = new Function();
			function.setName("����տѧ��蹷������ö���ҧ ������");
			functions.add(function);
		}
		
		return functions;
	}

}
