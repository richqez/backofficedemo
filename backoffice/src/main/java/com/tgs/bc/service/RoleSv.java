package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.FunctionWorkRightDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Permission;
import com.tgs.bc.model.Role;

public interface RoleSv extends Serializable{
	
	public List<Role> findByCriteria (Role role);
	
	public MsgStatus saveRolePermissions(Role role ,List<FunctionWorkRightDto> dtos);
	
	public List<Permission> findPermissionByRole(Role role);

	
}
