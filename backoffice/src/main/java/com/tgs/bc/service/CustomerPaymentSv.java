package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.CustomerPaymentCashDto;
import com.tgs.bc.dto.CustomerPaymentCheckDto;
import com.tgs.bc.dto.CustomerPaymentTransferDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.CustomerPayment;
import com.tgs.bc.model.TaxReceipt;

public interface CustomerPaymentSv extends Serializable {

	public MsgStatus saveCustomerPayment(CustomerPayment customerPayment,
			CustomerPaymentCashDto customerPaymentCashDto,
				CustomerPaymentTransferDto customerPaymentTransferDto,
					CustomerPaymentCheckDto customerPaymentCheckDto);
	public List<CustomerPayment> findByCriteria(CustomerPayment customerPayment);
	public void updateCustomerPayment(CustomerPayment customerPayment,
			CustomerPaymentCashDto customerPaymentCashDto,
				CustomerPaymentTransferDto customerPaymentTransferDto,
					CustomerPaymentCheckDto customerPaymentCheckDto);
	public Boolean CheckTaxReceipt(CustomerPayment customerPayment, CustomerPaymentCashDto customerPaymentCashDto,
			CustomerPaymentTransferDto customerPaymentTransferDto,
			CustomerPaymentCheckDto customerPaymentCheckDto, TaxReceipt taxReceipt);
}
