package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.BillDao;
import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Bill;
import com.tgs.bc.model.Invoice;

@Service
@Transactional
public class BillSvImpl implements BillSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3053873862177777327L;

	@Autowired
	BillDao billDao;

	@Autowired
	CommonDao commonDao;

	public List<Bill> findByCriteria(Bill bill) {
		return billDao.findByCriteria(bill);
	}


	public MsgStatus saveBillAndInvoice(Bill bill, List<Invoice> listOfInvoice,List<Invoice> listUpdate) {

		try {

			
	
			for (Invoice iv : listOfInvoice) {
				iv.setBill(bill);
				commonDao.saveAndUpdate(iv);
			}
			
			bill.setGrandTotal(calTotal(listOfInvoice));
			
			listUpdate.forEach(ivup->{
				ivup.setBill(null);
				commonDao.saveAndUpdate(ivup);
			});
			
			commonDao.saveAndUpdate(bill);
			
			return new MsgStatus.Builder("ทำการบันทึกข้อมูล "+bill.getBillNumber()+"สำเร็จ", true)
			.create();

			
		} catch (Exception e) {
			e.printStackTrace();
			
			return new MsgStatus.Builder("การบันทึกข้อมูลล้มเหลว", false)
			.create();
		}

	}

	
	private Double calTotal(List<Invoice> list) {
		Double total = 0.0;
		for (Invoice invoice : list) {
			total += invoice.getGrandTotal();
		}

		return total;
	}

	public List<Invoice> findInvoiceByBill(Bill bill) {
		return billDao.findInvoiceByBill(bill);
	}

	public List<Bill> findByCriteria(CommonSearchDto<Bill> commonSearchDto) {
		return billDao.findByCriteria(commonSearchDto);
	}

}
