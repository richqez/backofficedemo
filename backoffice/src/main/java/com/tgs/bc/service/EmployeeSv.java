package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.model.EmployeeInfo;

public interface EmployeeSv extends Serializable  {

	public List<EmployeeInfo> findByCriteria (EmployeeInfo employeeInfo);
	
}
