package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Role;
import com.tgs.bc.model.User;
import com.tgs.bc.model.UserRoles;

public interface UserSv extends Serializable {

	public List<User> findByCriteria(User user);
	
	public User login(User user);
	
	public MsgStatus saveUserRoles(User user,List<Role> roles); 
	
	public List<UserRoles> findByCriteriaRole(User user) ;
		
		
	
}
