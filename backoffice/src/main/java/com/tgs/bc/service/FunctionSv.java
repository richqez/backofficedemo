package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.model.Function;

public interface FunctionSv extends Serializable {
	public List<Function> findByCriteria (Function function);
	public List<Function> getAllNotInMenu();
}
