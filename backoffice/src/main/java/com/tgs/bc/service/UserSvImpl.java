package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dao.UserDao;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Role;
import com.tgs.bc.model.User;
import com.tgs.bc.model.UserRoles;

@Service
@Transactional
public class UserSvImpl implements UserSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2294966132713296274L;

	@Autowired
	UserDao userDao;

	@Autowired
	CommonDao commonDao;

	public List<User> findByCriteria(User user) {
		return userDao.findByCriteria(user);
	}

	public User login(User user) {
		return userDao.login(user);
	}

	public List<UserRoles> findByCriteriaRole(User user) {
		return userDao.findByCriteriaRole(user);

	}

	public MsgStatus saveUserRoles(User user, List<Role> roles) {

		try {

			commonDao.saveAndUpdate(user);

			if (!user.getUserRoleses().isEmpty()) {
				for (UserRoles u : user.getUserRoleses()) {
					commonDao.delete(u);
				}

			}

			for (Role role : roles) {

				UserRoles userRoles = new UserRoles();

				userRoles.setRole(role);
				userRoles.setUser(user);

				commonDao.saveAndUpdate(userRoles);

			}
			
			return new MsgStatus.Builder("ทำการบันทึกข้อมูล "+user.getUserName()+"สำเร็จ", true)
			.create();

		} catch (Exception e) {
			e.printStackTrace();
			
			return new MsgStatus.Builder("การบันทึกข้อมูลล้มเหลว", false)
			.create();
		}

	}

}
