package com.tgs.bc.service;

import java.util.List;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dao.QuotationDao;
import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Quotation;
import com.tgs.bc.model.QuotationDetail;


@Service
@Transactional
public class QuotationSvImpl implements QuotationSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 832131319986640515L;

	@Autowired
	QuotationDao quotationDao ;
	
	@Autowired
	CommonDao	commonDao;
	
	
	private Double discount;
	
	public List<Quotation> findByCriteria(Quotation quotation) {
		return quotationDao.findByCriteria(quotation);
	}


	public List<QuotationDetail> findQuotationDetailByQuotation(
			Quotation quotation) {
		return quotationDao.findQuotationDetailByQuotation(quotation);
	}


	
	public MsgStatus saveQuotationDetail(Quotation quotation,
			List<QuotationDetail> listOfQuotationDetail) {
		
		try {
			
			commonDao.saveAndUpdate(quotation);
		
			if (!quotation.getQuotationDetails().isEmpty()) {
				for (QuotationDetail quotationDetail : quotation.getQuotationDetails()) {
					commonDao.delete(quotationDetail);
				}
			}
			
				for (QuotationDetail qtDetail : listOfQuotationDetail) {

					QuotationDetail quotationDetail = new QuotationDetail();
					
					
					quotationDetail.setQuotation(quotation);
					quotationDetail.setAmount(qtDetail.getAmount());
					quotationDetail.setDescription(qtDetail.getDescription());
					quotationDetail.setEndDate(qtDetail.getEndDate());
					quotationDetail.setPeriodDescription(qtDetail.getPeriodDescription());
					quotationDetail.setProjectName(qtDetail.getProjectName());
					quotationDetail.setQuantity(qtDetail.getQuantity());
					quotationDetail.setStartDate(qtDetail.getStartDate());
					quotationDetail.setUnitPrice(qtDetail.getUnitPrice());
					quotationDetail.setUnitType(qtDetail.getUnitType());
					
					
				commonDao.saveAndUpdate(quotationDetail);
			}
				return new MsgStatus.Builder("ทำการบันทึกข้อมูล "+quotation.getQtNumber()+"สำเร็จ", true)
				.create();
				
		} catch (Exception e) {
			e.printStackTrace();
			
			return new MsgStatus.Builder("การบันทึกข้อมูลล้มเหลว", false)
			.create();

		}
		
	
	}


	public List<Quotation> findByCriteria(
			CommonSearchDto<Quotation> commonSearchDto) {
		return quotationDao.findByCriteria(commonSearchDto);
	}

	
	public Quotation calTotal(List<QuotationDetail> list, Quotation quotation) {
			quotation.setTotal(0.0);
		for (QuotationDetail quotationDetail : list) {
			quotation.setTotal(quotationDetail.getAmount()+quotation.getTotal());
		}	
		
		return quotation;
	}
	
	
	private Double calDiscount (Double discountPercentage,Double total){
		
		discount = (discountPercentage/100)*total ;
		
		return discount;
	}
	
	private Double calVat(Double vat,Double varPer,Double total,Double discount){
		
		vat = (varPer/100)*(total-discount) ;
		
		return vat ;
	}
	
	
	public Quotation calGrandtotal(Quotation quotation) {

		
		if (quotation.getVatPercentage()==null && quotation.getDiscountPercentage()== null) {
			quotation.setVatPercentage(0.0);
			quotation.setDiscountPercentage(0.0);
		}else if (quotation.getVatPercentage()==null) {
			quotation.setVatPercentage(0.0);
		}else if (quotation.getDiscountPercentage()==null) {
			quotation.setDiscountPercentage(0.0);
		}
		quotation.getTotal();
		
		quotation.setDiscount(
				calDiscount(quotation.getDiscountPercentage(), quotation.getTotal()));
		quotation.setVat(
				calVat(quotation.getVat(), quotation.getVatPercentage(), 
						quotation.getTotal(), quotation.getDiscount()));
		
		quotation.setGrandTotal(
				(quotation.getTotal()-quotation.getDiscount())+(quotation.getVat()) );
		
		return quotation;
	}


	@Override
	public List<Quotation> findQuotationByCustomer(Customer customer) {
		return quotationDao.findQuotationByCustomer(customer);
	}




}
