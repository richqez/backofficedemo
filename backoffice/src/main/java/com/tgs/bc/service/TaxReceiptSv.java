package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.TaxReceipt;
import com.tgs.bc.model.TaxReceiptDetail;

/**
 * @author Taiy
 *
 */
public interface TaxReceiptSv extends Serializable {

	public List<TaxReceipt> findByCritetia(
			CommonSearchDto<TaxReceipt> commonSearchDto);

	public void calculate(TaxReceipt receipt, List<TaxReceiptDetail> details);

	MsgStatus saveTaxReceiptAndDetail(TaxReceipt taxReceipt,
			List<TaxReceiptDetail> listTaxReceiptDetails,
			List<Invoice> listOfInvoices, List<Invoice> listOfInvoicesForUpdate);

	public MsgStatus deleteTexReceipt(TaxReceipt taxReceipt);

}
