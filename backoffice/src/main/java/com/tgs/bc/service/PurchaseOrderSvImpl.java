package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dao.PurchaseOrderDao;
import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.PurchaseOrder;
import com.tgs.bc.model.PurchaseOrderDetail;

@Service
public class PurchaseOrderSvImpl implements PurchaseOrderSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4002981319001288421L;

	@Autowired
	CommonDao commonDao;

	@Autowired
	PurchaseOrderDao purchaseOrderDao;


	/*
	 * method for save and update
	 * 
	 * case add => test : success
	 */
	@Transactional
	public MsgStatus savePurchaseOrderAndDetail(PurchaseOrder po,
			List<PurchaseOrderDetail> details) {

		try {

			if (!po.getPurchaseOrderDetails().isEmpty()) {

				po.getPurchaseOrderDetails().forEach(
						pod -> commonDao.delete(pod));

				po.setPurchaseOrderDetails(null);

			}

			commonDao.saveAndUpdate(po);

			for (PurchaseOrderDetail purchaseOrderDetail : details) {
				purchaseOrderDetail.setPurchaseOrder(po);
				purchaseOrderDetail.setId(0);
				commonDao.saveAndUpdate(purchaseOrderDetail);
			}

			return new MsgStatus.Builder("ทำการบันทึกข้อมูล "+po.getPoNumber()+"สำเร็จ", true)
					.create();

		} catch (Exception e) {

			e.printStackTrace();
			
			return new MsgStatus.Builder("การบันทึกข้อมูลล้มเหลว", false)
					.create();

		}

	}

	/*
	 * search by criteria
	 * 
	 * case search by StartDate or EndDate => Test : success case search by
	 * PurchaseOrderObject => Test : ?
	 */
	@Transactional
	public List<PurchaseOrder> findByCriteria(
			CommonSearchDto<PurchaseOrder> commonSearchDto) {

		return purchaseOrderDao.findByCriteria(commonSearchDto);
	}

	@Override
	public void calculatePurchaseOrder(PurchaseOrder purchaseOrder,
			List<PurchaseOrderDetail> purchaseOrderDetails) {
		if (!purchaseOrderDetails.isEmpty()) {

			double total = 0;
			double vat = 0;
			double grandtotal = 0;

			// calculate PurchaseOrderDetail amount = price * qty
			purchaseOrderDetails.forEach(pod -> {
				pod.setAmount(pod.getQuantity() * pod.getUnitPrice());
			});

			total = purchaseOrderDetails.stream()
					.mapToDouble(x -> x.getAmount()).sum();

			vat = (total * 7) / 100;

			grandtotal = total + vat;

			purchaseOrder.setTotal(total);
			purchaseOrder.setVat(vat);
			purchaseOrder.setGrandTotal(grandtotal);

		}

	}
	
	@Transactional
	@Override
	public List<PurchaseOrder> findAllPurchaseOrderAndDetail() {
		return purchaseOrderDao.findAllPurchaseOrderAndDetail();
	}

}
