package com.tgs.bc.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dao.TaxReceiptDao;
import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.TaxReceipt;
import com.tgs.bc.model.TaxReceiptDetail;

/**
 * @author Taiy
 *
 */
@Service
public class TaxReceiptSvImpl implements TaxReceiptSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8391020504396331989L;

	@Autowired
	TaxReceiptDao taxReceiptDao;

	@Autowired
	CommonDao commonDao;

	MsgStatus msgStatus;

	double sumOflistOfExistInvoice;

	double taxReceiptTotal;

	@Transactional
	public List<TaxReceipt> findByCritetia(
			CommonSearchDto<TaxReceipt> commonSearchDto) {
		return taxReceiptDao.findByCriteria(commonSearchDto);
	}

	private void calTotal(TaxReceipt receipt, List<TaxReceiptDetail> details) {

		double result = 0;

		for (TaxReceiptDetail taxReceiptDetail : details) {
			result += taxReceiptDetail.getUnitPrice()
					* taxReceiptDetail.getQuantity();
			taxReceiptDetail.setAmount(taxReceiptDetail.getUnitPrice()
					* taxReceiptDetail.getQuantity());
		}

		receipt.setTotal(result);

	}

	private void calGrandTotal(TaxReceipt receipt) {
		receipt.setGrandTotal(receipt.getTotal() + receipt.getVat());
	}

	private void calVat(TaxReceipt receipt) {

		receipt.setVat((receipt.getTotal() * 7) / 100);

	}

	public boolean isLessThanSumOfInvoice(TaxReceipt receipt,
			List<Invoice> listOfInvoices) {

		double sumOfInvoice = listOfInvoices.stream()
				.mapToDouble(iv -> iv.getTotal()).sum();

		this.taxReceiptTotal = receipt.getTotal();
		this.sumOflistOfExistInvoice = sumOfInvoice;

		return sumOfInvoice <= receipt.getTotal() ? true : false;

	}

	public void calculate(TaxReceipt receipt, List<TaxReceiptDetail> details) {

		calTotal(receipt, details);
		calVat(receipt);
		calGrandTotal(receipt);

	}

	@Transactional
	public MsgStatus saveTaxReceiptAndDetail(TaxReceipt taxReceipt,
			List<TaxReceiptDetail> listTaxReceiptDetails,
			List<Invoice> listOfInvoices, List<Invoice> listOfInvoicesForUpdate) {

		calculate(taxReceipt, listTaxReceiptDetails);

		if (isLessThanSumOfInvoice(taxReceipt, listOfInvoices)) {

			if (!taxReceipt.getTaxReceiptDetails().isEmpty()) {
				taxReceipt.getTaxReceiptDetails().forEach(txrd -> {
					commonDao.delete(txrd);
				});
			}

			commonDao.saveAndUpdate(taxReceipt);

			listTaxReceiptDetails.forEach(txrd -> {
				txrd.setTaxReceipt(taxReceipt);
				commonDao.saveAndUpdate(txrd);

			});

			listOfInvoices.forEach(iv -> {

				iv.setTaxReceipt(taxReceipt);
				commonDao.saveAndUpdate(iv);

			});

			if (taxReceipt.getId() != 0) {

				listOfInvoicesForUpdate.forEach(iv -> {
					iv.setTaxReceipt(null);
					commonDao.saveAndUpdate(iv);
				});

			}

			return new MsgStatus.Builder("ทำการบันทึกข้อมูลสำเร็ต", true)
					.create();
		}

		else {
			return new MsgStatus.Builder(
					"ยอดเงินเกิน  เพราะ  ยอดรวมของ invoce คือ"
							+ sumOflistOfExistInvoice
							+ "แต่ยอดเงิน ของ TaxReceipt คือ" + taxReceiptTotal,
					false).create();
		}

	}

	@Transactional
	public MsgStatus deleteTexReceipt(TaxReceipt taxReceipt) {

		try {

			if (!taxReceipt.getInvoices().isEmpty()) {
				taxReceipt.getInvoices().forEach(inv -> {
					inv.setTaxReceipt(null);
					commonDao.saveAndUpdate(inv);
				});

			}

			commonDao.delete(taxReceipt);

			return new MsgStatus.Builder("ทำการลบ TaxReceipt "
					+ taxReceipt.getTaxReceiptNumber(), true).create();

		} catch (Exception e) {

			return new MsgStatus.Builder("ไม่สามารถลบข้อมูลได้  "+e.getMessage(), false)
					.create();

		}

	}

}
 