package com.tgs.bc.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dao.InvoiceDao;
import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Customer;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.PurchaseOrder;

@Service
public class InvoiceSvImpl implements InvoiceSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7238484379647310619L;

	@Autowired
	InvoiceDao invoiceDao;

	@Autowired
	CommonDao commonDao;

	MsgStatus msgStatus;
	
	double currentInvoiceTotal ;
	
	double purchaseOrderTotal ;
	
	double sumOfExistInvoice ;
	

	/*
	 * method for calculate all field in invoice
	 * 
	 * Test : success
	 */
	public Invoice calculateInvoice(Invoice invoice,
			List<InvoiceDetail> invoiceDetails) {

		calInvoidDetailAmount(invoiceDetails);
		calInvoiceTotal(invoice, invoiceDetails);
		calVat(invoice);
		calGrandTotal(invoice);

		return invoice;
	}

	/*
	 * calculate invoice grand total return result and set value in invoice
	 * object -- grand total = vat - total
	 * 
	 * Test : success
	 */
	private double calGrandTotal(Invoice invoice) {
		double result = invoice.getTotal() - invoice.getVat();
		invoice.setGrandTotal(result);
		System.out.println("invoice cal grand total : " + result);
		return result;

	}

	/*
	 * calculate invoice vat return result and set value in invoice object --
	 * hard code percentage for calculate 7%
	 * 
	 * Test : success
	 */
	private double calVat(Invoice invoice) {
		double result = (invoice.getTotal() * 7) / 100;
		invoice.setVat(result);
		System.out.println("invoice cal vat : " + result);
		return result;
	}

	/*
	 * calculate invoice total return result and set value in invoice object --
	 * calculate by sum price multiple quantity all item
	 * 
	 * Test : success
	 */
	private double calInvoiceTotal(Invoice invoice,
			List<InvoiceDetail> invoiceDetails) {
		double result = 0;
		for (InvoiceDetail ivd : invoiceDetails) {
			result += ivd.getAmount();
		}
		invoice.setTotal(result);

		System.out.println("invoice cal total : " + result);
		return result;
	}

	/*
	 * calculate invoice detail and set value to current item
	 * 
	 * Test : success
	 */
	private void calInvoidDetailAmount(List<InvoiceDetail> invoiceDetails) {
		for (InvoiceDetail invoiceDetail : invoiceDetails) {
			invoiceDetail.setAmount(invoiceDetail.getUnitPrice()
					* invoiceDetail.getQuantity());
		}

	}

	/*
	 * validation Condition => invoice should not greater than sum of purchase
	 * order for bundle
	 * 
	 * Note :: Now i can't do it
	 */
	@Transactional
	public boolean isLessThanSumOfPurchaseOrder(Invoice invoice,
			PurchaseOrder purchaseOrder) {

		List<Invoice> listExistInvoice = invoiceDao
				.findInvoiceByPurchaseOrder(purchaseOrder);

		if (listExistInvoice.size() < 1
				&& invoice.getGrandTotal() <= purchaseOrder.getTotal()) {
			return true;
		}

		// Filter data for case update
		listExistInvoice = listExistInvoice.stream()
				.filter(inv -> inv.getId() != invoice.getId())
				.collect(Collectors.toList());

		double currentInvoiceTotal = invoice.getTotal();
		double purchaseOrderTotal = purchaseOrder.getTotal();
		double sumOfExistInvoice = 0;
		
		this.currentInvoiceTotal = currentInvoiceTotal;
		this.purchaseOrderTotal = purchaseOrderTotal ;
		

		sumOfExistInvoice = listExistInvoice.stream()
				.mapToDouble(inv -> inv.getTotal()).sum();
		
		this.sumOfExistInvoice = sumOfExistInvoice;

		System.out.println("sumOfExistInvoice" + sumOfExistInvoice);
		System.out.println("CurrentInvoice" + currentInvoiceTotal);

		return (sumOfExistInvoice + currentInvoiceTotal) <= purchaseOrderTotal ? true
				: false;
	}



	/*
	 * method for save or update
	 * 
	 * case add => Test : success case update => Test : ?
	 */
	@Transactional
	public MsgStatus saveInvoiceAndInvoiceDetail(Invoice invoice,
			List<InvoiceDetail> detail, PurchaseOrder purchaseOrder) {

		calculateInvoice(invoice, detail);

		if (isLessThanSumOfPurchaseOrder(invoice, purchaseOrder)) {

			System.out.println("isLessThanSumOfPurchaseOrder : pass!!!!");
			
			if (!invoice.getInvoiceDetails().isEmpty()) {
				invoice.getInvoiceDetails().forEach(
						ivd -> commonDao.delete(ivd));
			}

			commonDao.saveAndUpdate(invoice);

			detail.forEach(ivd -> {
				ivd.setId(0);
				ivd.setInvoice(invoice);
				commonDao.saveAndUpdate(ivd);
			});

			return new MsgStatus.Builder("ทำการบันทึก ข้อมูลสำเร็จ", true).create();
			
		} else {
			System.out.println("isLessThanSumOfPurchaseOrder :fail");
			
			return new MsgStatus.Builder("ไม่สามารถบันทึกข้อมูได้เนื่องจาก   InvoiceTotal ยอด :" + this.currentInvoiceTotal + 
					" ซึ่ง PurchaseOrderรายการนี้มียอด :" + this.purchaseOrderTotal + " ซึ่งถูกเรียกเก็บเงินไปแล้ว  :"
					+this.sumOfExistInvoice,false).create();
		}

	}

	/*
	 * Search by Criteria
	 * 
	 * case Search by StartDate or EndDate => Test : success case search by
	 * InvoiceObject => Test : ?
	 */
	@Transactional
	public List<Invoice> findByCriteria(CommonSearchDto<Invoice> searchDto) {
		return invoiceDao.findByCriteria(searchDto);
	}

	/*
	 * method approved invoice update invoice field status Now i don't know
	 * status code
	 */
	@Override
	public MsgStatus approved(Invoice invoice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MsgStatus unApproved(Invoice invoice) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public List<Invoice> findInvoiceByCustomer(Customer customer) {
		return invoiceDao.findInvoiceByCustomer(customer);
	}

}
