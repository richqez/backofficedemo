package com.tgs.bc.service;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dao.CustomerPaymentDao;
import com.tgs.bc.dto.CustomerPaymentCashDto;
import com.tgs.bc.dto.CustomerPaymentCheckDto;
import com.tgs.bc.dto.CustomerPaymentTransferDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.CustomerPayment;
import com.tgs.bc.model.TaxReceipt;

@Service
@Transactional
public class CustomerPaymentSvImpl implements CustomerPaymentSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2437148642060408311L;

	@Autowired
	CommonDao commonDao;

	@Autowired
	CustomerPaymentDao customerPaymentDao;

	public MsgStatus saveCustomerPayment(CustomerPayment customerPayment,
			CustomerPaymentCashDto customerPaymentCashDto,
			CustomerPaymentTransferDto customerPaymentTransferDto,
			CustomerPaymentCheckDto customerPaymentCheckDto) {

		if (customerPayment.getCustomerPaymentName().equals("cash")) {

			customerPayment.setAmount(customerPaymentCashDto.getAmount());
			customerPayment.setAccountDate(customerPaymentCashDto
					.getAccountDate());
			customerPayment.setWithHoldingTax(customerPaymentCashDto
					.getWithHoldingTax());

		} else if (customerPayment.getCustomerPaymentName().equals("transfer")) {

			customerPayment.setAmount(customerPaymentTransferDto.getAmount());
			customerPayment.setFee(customerPaymentTransferDto.getFee());
			customerPayment.setAccountDate(customerPaymentTransferDto
					.getAccountDate());
			customerPayment.setWithHoldingTax(customerPaymentTransferDto
					.getWithHoldingTax());

		} else if (customerPayment.getCustomerPaymentName().equals("check")) {

			customerPayment.setAmount(customerPaymentCheckDto.getAmount());
			customerPayment.setWithHoldingTax(customerPaymentCheckDto
					.getWithHoldingTax());
			customerPayment.setDepositBalance(customerPaymentCheckDto
					.getDepositBalance());
			customerPayment.setDepositAccount(customerPaymentCheckDto
					.getDepositAccount());
			customerPayment.setDepositDate(customerPaymentCheckDto
					.getDepositDate());
			customerPayment.setBank(customerPaymentCheckDto.getBank());
			customerPayment.setChcekNo(customerPaymentCheckDto.getChcekNo());
			customerPayment
					.setCheckDate(customerPaymentCheckDto.getCheckDate());
			customerPayment.setAccountDate(customerPaymentCheckDto
					.getAccountDate());

		}
		commonDao.saveAndUpdate(customerPayment);

		return new MsgStatus.Builder("ทำการบันทึกข้อมูล "
				+ customerPayment.getTaxReceipt().getTaxReceiptNumber()
				+ "สำเร็จ", true).create();
	}

	public List<CustomerPayment> findByCriteria(CustomerPayment customerPayment) {
		return customerPaymentDao.findByCriteria(customerPayment);
	}

	public void updateCustomerPayment(CustomerPayment customerPayment,
			CustomerPaymentCashDto customerPaymentCashDto,
			CustomerPaymentTransferDto customerPaymentTransferDto,
			CustomerPaymentCheckDto customerPaymentCheckDto) {

		if (customerPayment.getCustomerPaymentName().equals("cash")) {

			customerPaymentCashDto.setAmount(customerPayment.getAmount());
			customerPaymentCashDto.setAccountDate(customerPayment
					.getAccountDate());
			customerPaymentCashDto.setWithHoldingTax(customerPayment
					.getWithHoldingTax());

		} else if (customerPayment.getCustomerPaymentName().equals("transfer")) {
			customerPaymentTransferDto.setAccountDate(customerPayment
					.getAccountDate());
			customerPaymentTransferDto.setAmount(customerPayment.getAmount());
			customerPaymentTransferDto.setWithHoldingTax(customerPayment
					.getWithHoldingTax());
			customerPaymentTransferDto.setFee(customerPayment.getFee());

		} else {
			customerPaymentCheckDto.setAccountDate(customerPayment
					.getAccountDate());
			customerPaymentCheckDto.setAmount(customerPayment.getAmount());
			customerPaymentCheckDto.setBank(customerPayment.getBank());
			customerPaymentCheckDto.setChcekNo(customerPayment.getChcekNo());
			customerPaymentCheckDto
					.setCheckDate(customerPayment.getCheckDate());
			customerPaymentCheckDto.setDepositAccount(customerPayment
					.getDepositAccount());
			customerPaymentCheckDto.setDepositBalance(customerPayment
					.getDepositBalance());
			customerPaymentCheckDto.setDepositDate(customerPayment
					.getDepositDate());
			customerPaymentCheckDto.setWithHoldingTax(customerPayment
					.getWithHoldingTax());
		}

	}

	@SuppressWarnings("unchecked")
	public Boolean CheckTaxReceipt(CustomerPayment customerPayment,
			CustomerPaymentCashDto customerPaymentCashDto,
			CustomerPaymentTransferDto customerPaymentTransferDto,
			CustomerPaymentCheckDto customerPaymentCheckDto,
			TaxReceipt taxReceipt) {

		Double total = 0.0;
		List<CustomerPayment> customerPayments = (List<CustomerPayment>) commonDao
				.findAll(CustomerPayment.class);
		for (CustomerPayment cPayment : customerPayments) {

			if (customerPayment.getId() != 0) {
				total = cPayment.getAmount() - cPayment.getAmount();

			} else {

				if (customerPayment.getTaxReceipt().getId() == cPayment
						.getTaxReceipt().getId()) {
					total = total + cPayment.getAmount();
				}

			}

			if (customerPayment.getCustomerPaymentName().equals("cash")) {
				if (customerPaymentCashDto.getAmount() + total > taxReceipt
						.getGrandTotal()) {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_WARN,
									"จำนวนเงินที่ป้อน "
											+ customerPaymentCashDto
													.getAmount()
											+ "รวมกับยอดปัจจุบัน " + total
											+ "เกินยอดใบกำกับภาษี "
											+ taxReceipt.getGrandTotal(), ""));

					return false;
				}

			} else if (customerPayment.getCustomerPaymentName().equals(
					"transfer")) {
				if (customerPaymentTransferDto.getAmount() + total > taxReceipt
						.getGrandTotal()) {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_WARN,
									"จำนวนเงินที่ป้อน "
											+ customerPaymentTransferDto
													.getAmount()
											+ "รวมกับยอดปัจจุบัน " + total
											+ "เกินยอดใบกำกับภาษี "
											+ taxReceipt.getGrandTotal(), ""));
					return false;
				}

			} else if (customerPayment.getCustomerPaymentName().equals("check")) {

				if (customerPaymentCheckDto.getAmount() + total > taxReceipt
						.getGrandTotal()) {
					FacesContext.getCurrentInstance().addMessage(
							null,
							new FacesMessage(FacesMessage.SEVERITY_WARN,
									"จำนวนเงินที่ป้อน "
											+ customerPaymentCheckDto
													.getAmount()
											+ "รวมกับยอดปัจจุบัน " + total
											+ "เกินยอดใบกำกับภาษี "
											+ taxReceipt.getGrandTotal(), ""));
					return false;

				}
			}
		}
		return true;

	}

}
