package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.model.Position;

public interface PositionSv extends Serializable {

	public List<Position> findByCriteria (Position position);
	
}
