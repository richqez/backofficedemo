package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CustomerDao;
import com.tgs.bc.model.Customer;

@Service
@Transactional
public class CustomerSvImpl implements CustomerSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6747097987871907777L;
	@Autowired
	CustomerDao customerDao;

	public List<Customer> findByCriteria(Customer customer) {
		
		return customerDao.findByCriteria(customer);
	}

	
	public List<Customer> findByCriteriaById(Customer customer) {
		return customerDao.findByCriteriaById(customer);
	}

	
	
	

}
