/**
 * 
 */
package com.tgs.bc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.CommonDao;
import com.tgs.bc.dto.MsgStatus;

/**
 * @author Taiy
 *
 */
@Service
@Transactional
public class CommonSvImpl implements CommonSv {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6806191450929684418L;

	@Autowired
	CommonDao commonDao;

	MsgStatus msgStatus;

	public MsgStatus saveAndUpdate(Object object) {

		msgStatus = new MsgStatus();
		try {

			commonDao.saveAndUpdate(object);
			msgStatus.setStatus(true);
			msgStatus.setStatusStr("บันทึกข้อมูลสำเร็จ");

		} catch (Exception e) {
			msgStatus.setStatus(false);
			msgStatus.setStatusStr("บันทึกข้อมูลไม่สำเร็จ เนื่องจาก"
					+ e.getMessage());
		}

		return msgStatus;

	}

	public MsgStatus delete(Object object) {
		
		msgStatus = new MsgStatus();
		
		try {
			commonDao.delete(object);
			msgStatus.setStatus(true);
			msgStatus.setStatusStr("ลบข้อมูลสำเร็จ");
		} catch (Exception e) {
			msgStatus.setStatus(false);
			msgStatus.setStatusStr("บันทึกข้อมูลไม่สำเร็จ เนื่องจาก"
					+ e.getMessage());
		}
		return msgStatus;
	}

	public List<?> findAll(Class<?> clazz) {
		return commonDao.findAll(clazz);
	}

	public Object findById(Class<?> clazz, int id) {
		return commonDao.findById(clazz, id);
	}

	public Object findById(Class<?> clazz, String id) {
		return commonDao.findById(clazz, id);
	}


	public Object findByIdCode(Class<?> clazz, String id) {
		return commonDao.findByIdCode(clazz, id);
	}

	
	public List<?> findAllOrderBy(Class<?> clazz, String id) {
		return commonDao.findAllOrderBy(clazz, id);
	}










































}
