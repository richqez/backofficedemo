package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.dto.MsgStatus;
import com.tgs.bc.model.Bill;
import com.tgs.bc.model.Invoice;

public interface BillSv extends Serializable {
	public List<Bill> findByCriteria(Bill bill);
	public MsgStatus saveBillAndInvoice(Bill	bill ,List<Invoice> listOfInvoice,List<Invoice> listUpdate);
	public List<Invoice> findInvoiceByBill(Bill bill);
	public List<Bill> findByCriteria(CommonSearchDto<Bill> commonSearchDto);
}
