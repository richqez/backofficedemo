package com.tgs.bc.service;

import java.io.Serializable;
import java.util.List;

import com.tgs.bc.model.Menu;

public interface MenuSv extends Serializable {
	public List<Menu> getAllMenuNotLv3();
	
	
	public List<Menu> getAll();
}
