package backoffice;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.bc.dao.TaxReceiptDao;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.TaxReceipt;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
@Transactional
public class TaxReceiptDaoTest {

	@Autowired
	TaxReceiptDao dao;
	
	@Test
	public void testFindByInvoice(){
		
		Invoice invoice = new Invoice();
		
		invoice.setId(1);
		
		
		List<TaxReceipt> taxReceipts = dao.findByInvoice(invoice);
		
		System.out.print("size" + taxReceipts.size());
		
		taxReceipts.forEach(x->{
			
			System.out.println(x.getId());
			System.out.println(x.getTaxReceiptNumber());
			
			
		});
		
	}
	
	
	
}
