package backoffice;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tgs.bc.dto.CommonSearchDto;
import com.tgs.bc.model.Invoice;
import com.tgs.bc.model.InvoiceDetail;
import com.tgs.bc.model.PurchaseOrder;
import com.tgs.bc.service.CommonSv;
import com.tgs.bc.service.InvoiceSv;

/*
 *  Please add @Test on the method for test it
 * 
 * 
 * 
 * */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class InvoiceServiceTest {

	@Autowired
	InvoiceSv invoiceSv;
	
	@Autowired
	CommonSv commonSv ;

	Invoice invoice;

	InvoiceDetail detail;

	List<InvoiceDetail> invoiceDetails;

	public void searchTest() {

		List<Invoice> invoices = new ArrayList<Invoice>();

		CommonSearchDto<Invoice> commonSearchDto = new CommonSearchDto<Invoice>();

		Hashtable<String, Date> criteria = new Hashtable<String, Date>();

		criteria.put(CommonSearchDto.START_DATE, new Date());

		commonSearchDto.setCriteria(criteria);

		invoices = invoiceSv.findByCriteria(commonSearchDto);

		System.out.println("Search result  is :  " + invoices.size());

		invoices.forEach(ivd -> {

			System.out.println(ivd.getInvoiceNumber());

		});

	}
	@Test
	public void calculateTest() {
		
		CommonSearchDto<Invoice> commonSearchDto = new CommonSearchDto<Invoice>();
		
		Invoice sInvoice = new Invoice();
		sInvoice.setId(5);
		
		commonSearchDto.setObject(sInvoice);
		commonSearchDto.setCriteria(new Hashtable<String, Date>());
		
		invoice = invoiceSv.findByCriteria(commonSearchDto).get(0);

		invoiceDetails = new ArrayList<InvoiceDetail>();
		
		detail = new InvoiceDetail();

		detail.setUnitPrice((double) 100);
		detail.setQuantity((double) 10);
		
		invoiceDetails.add(detail);

		detail = new InvoiceDetail();
		
		detail.setUnitPrice((double) 100);
		detail.setQuantity((double) 5);
		
		invoiceDetails.add(detail);

	
		PurchaseOrder purchaseOrder = (PurchaseOrder) commonSv.findById(PurchaseOrder.class,1);
		
		
		

		invoiceSv.saveInvoiceAndInvoiceDetail(invoice, invoiceDetails,
				purchaseOrder);

	}
	

}
